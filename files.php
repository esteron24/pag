<!DOCTYPE html>
<html lang="en">
<?php
    require 'connect.php';
?>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Performing Arts Group | Home</title>
    <link rel="shortcut icon" href="_landing_assets/img/cvsu.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/fonts/line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/slicknav.css">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/nivo-lightbox.css" >
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/responsive.css">

  </head>
  <style>
      .nav-link{
          color: black !important;
      }
  </style>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <a href="index.php" class="navbar-brand"><img style="width: 20%; height: auto;" src="_landing_assets/img/cvsu.png" alt=""></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                <a class="nav-link" href="index.php">
                  Home
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Groups
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php
                        $group_query = "SELECT * FROM groups_tbl";
                        $group_fetch_result = mysqli_query($con, $group_query);
                        while($row = mysqli_fetch_array($group_fetch_result)) {
                            echo '<a class="dropdown-item" href="group_single.php?group='.$row['group_id'].'">'.$row['group_name'].'</a>';
                        }
                    ?>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">
                  About
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="events.php">
                  Events
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="news.php">
                  News
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="login.php">
                  Log In
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="index.html#team">
                  Contact
                </a>
              </li> -->
              <li class="nav-item active">
                <a class="nav-link" href="files.php">
                  Download
                </a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
          <li>
            <a class="page-scroll" href="index.php">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Groups
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php
                    $group_query = "SELECT * FROM groups_tbl";
                    $group_fetch_result = mysqli_query($con, $group_query);
                    while($row = mysqli_fetch_array($group_fetch_result)) {
                        echo '<a class="dropdown-item" href="group_single.php?group='.$row['group_id'].'">'.$row['group_name'].'</a>';
                    }
                ?>
            </div>
          </li>
          <li>
             <a class="page-scroll" href="about.php">About</a>
          </li>
          <li>
            <a class="page-scroll" href="events.php">Events</a>
          </li>
          <li>
            <a class="page-scroll" href="news.php">News</a>
          </li>
          <li>
            <a class="page-scroll" href="login.php">Log In</a>
          </li>
          <!-- <li>
            <a class="page-scroll" href="#team">Contact</a>
          </li> -->
          <li>
            <a class="page-scroll" href="files.php">Download</a>
          </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->
    </header>
    
    <!-- Forms Section Start -->
    <section style="margin-top: 30px;" id="event-slides" class="news-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Downloadable Forms</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Global Grand Event on Digital Design</p>
            </div>
            <ul class="list">
            <?php
               $form_query = "SELECT * FROM forms_tbl WHERE is_featured = 1";
               $forms_fetch_result = mysqli_query($con, $form_query);
               while($row = mysqli_fetch_array($forms_fetch_result)) {
                echo'
                  <li class="media my-4">
                    <img src="_landing_assets/img/file.png" style="height: 50px;" class="mr-3" alt="...">
                    <div class="media-body">
                      <h5 class="mt-0 mb-1">'.$row['form_name'].'</h5>
                      <a href="download.php?form='.$row['form_file'].'" class="download_link">Click here to download</a>
                    </div>
                  </li>
                ';
              }
            ?>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- Forms Slides Section End -->
    
    <?php include 'footer.php'; ?>

    <div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info">
              <p>© Designed and Developed by <a href="http://uideck.com" rel="nofollow">UIdeck</a></p>
            </div>      
          </div>
        </div>
      </div>
    </div>

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
    </a>

    <div id="preloader">
      <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
      </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="_landing_assets/js/jquery-min.js"></script>
    <script src="_landing_assets/js/popper.min.js"></script>
    <script src="_landing_assets/js/bootstrap.min.js"></script>
    <script src="_landing_assets/js/jquery.countdown.min.js"></script>
    <script src="_landing_assets/js/jquery.nav.js"></script>
    <script src="_landing_assets/js/jquery.easing.min.js"></script>
    <script src="_landing_assets/js/wow.js"></script>
    <script src="_landing_assets/js/jquery.slicknav.js"></script>
    <script src="_landing_assets/js/nivo-lightbox.js"></script>
    <script src="_landing_assets/js/main.js"></script>
    <script src="_landing_assets/js/form-validator.min.js"></script>
    <script src="_landing_assets/js/contact-form-script.min.js"></script>  
  </body>
</html>
