<?php
  session_start();
  if(isset($_POST['signin_btn'])){
    require 'connect.php';
    $username = $_POST['student_number'];
    
    $result = mysqli_query($con, 'SELECT * FROM account_tbl WHERE username = "'.$username.'"');
    $row = mysqli_fetch_assoc($result);
    $account = $row['account_id'];
    $access_level = $row['access_level_id'];

    if(password_verify($_POST['password'], $row['password'])) {
      if($access_level == 1){
        $_SESSION['session_admin_id'] = $account;
        header("location: ..\pag\admin\index.php");
      } elseif($access_level == 2) {
          $_SESSION['session_president_id'] = $account;
          header("location: ..\pag\president\index.php");
      } else {
          echo "<script>alert('There was an error in login attempt!');</script>";
      }  
    } else {
      echo "<script>alert('Student number is not registered!');</script>";
    } 
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in | Performing Arts Group </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="_assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="_assets/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Icon -->
  <link rel="shortcut icon" href="_assets/dist/img/cvsu.png">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index.html"><img style="width: 45%; height: auto;" src="_assets/dist/img/cvsu.png"></a>
    
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Log in to start your session</p>

      <form action="#" method="post">
        <div class="input-group mb-3">
          <input type="text" name="student_number" class="form-control" placeholder="Please enter your student number" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Please enter your password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" name="signin_btn" class="btn btn-success btn-block">Log In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <div class="social-auth-links text-center mb-3">
        Back to <a href="index.php">Home Page</a>
      </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="_assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="_assets/dist/js/adminlte.min.js"></script>

</body>
</html>
