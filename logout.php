<?php
	session_start();
	unset($_SESSION['session_admin_id']);
	unset($_SESSION['session_president_id']);
	session_destroy();
	header("location: login.php");
?>