<!DOCTYPE html>
<html lang="en">
<?php
    require 'connect.php';
    $group_id = $_GET['group'];
    $group_query="SELECT * FROM groups_tbl A WHERE A.group_id='".$group_id."'";
    $group_result = mysqli_query($con, $group_query);
    $group_fetch = mysqli_fetch_assoc($group_result);
    
    $group_name = $group_fetch['group_name'];
    $group_logo = $group_fetch['group_logo'];
    $cover_image = $group_fetch['cover_image'];
    $embed_link = $group_fetch['video_embed_link'];
    $group_description = $group_fetch['group_description'];
?>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?=$group_name;?> | Performing Arts Group</title>
    <link rel="shortcut icon" href="_landing_assets/img/cvsu.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/fonts/line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/slicknav.css">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/nivo-lightbox.css" >
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/responsive.css">
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/owl.theme.default.min.css">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <span class="icon-menu"></span>
                        <span class="icon-menu"></span>
                        <span class="icon-menu"></span>
                    </button>
                    <a href="index.php" class="navbar-brand"><img style="width: 20%; height: auto;" src="_landing_assets/img/cvsu.png" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                        <a class="nav-link" href="index.php">
                            Home
                        </a>
                        </li>
                        <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Groups
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php
                                $group_query = "SELECT * FROM groups_tbl";
                                $group_fetch_result = mysqli_query($con, $group_query);
                                while($row = mysqli_fetch_array($group_fetch_result)) {
                                    echo '<a class="dropdown-item" href="group_single.php?group='.$row['group_id'].'">'.$row['group_name'].'</a>';
                                }
                            ?>
                        </div>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="about.php">
                            About
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="events.php">
                            Events
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="news.php">
                            News
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="login.php">
                            Log In
                        </a>
                        </li>
                        <!-- <li class="nav-item">
                        <a class="nav-link" href="index.html#team">
                            Contact
                        </a>
                        </li> -->
                        <li class="nav-item">
                        <a class="nav-link" href="files.php">
                            Download
                        </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Mobile Menu Start -->
            <ul class="mobile-menu">
                <li>
                <a class="page-scroll" href="index.php">Home</a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="index.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Groups
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php
                        $group_query = "SELECT * FROM groups_tbl";
                        $group_fetch_result = mysqli_query($con, $group_query);
                        while($row = mysqli_fetch_array($group_fetch_result)) {
                            echo '<a class="dropdown-item" href="group_single.php?group='.$row['group_id'].'">'.$row['group_name'].'</a>';
                        }
                    ?>
                </div>
                </li>
                <li>
                    <a class="page-scroll" href="about.php">About</a>
                </li>
                <li>
                <a class="page-scroll" href="events.php">Events</a>
                </li>
                <li>
                <a class="page-scroll" href="news.php">News</a>
                </li>
                <li>
                <a class="page-scroll" href="login.php">Log In</a>
                </li>
                <!-- <li>
                <a class="page-scroll" href="#team">Contact</a>
                </li> -->
                <li>
                <a class="page-scroll" href="files.php">Download</a>
                </li>
            </ul>
            <!-- Mobile Menu End -->
        </nav>
        <!-- Navbar End -->
    
        <!-- Main Carousel Section Start -->
        <div id="main-slide" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 filter_brightness" src="_uploads/images/<?=$cover_image;?>" alt="First slide">
                    <div class="carousel-caption d-md-block">
                        <p class="fadeInUp wow" data-wow-delay=".6s">Cavite State University - Indang Campus</p>
                        <h1 class="wow fadeInDown heading" data-wow-delay=".4s">
                            <?=$group_name;?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Carousel Section End -->
    </header>

    <!-- Group Info Slides Section Start -->
    <section style="margin-top: 20px;" id="event-slides" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Who We Are?</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Know us more straight from us!</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-6 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
            <div class="video">
              <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="<?=$embed_link;?>" frameborder="0" allowfullscreen=""></iframe>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">
            <p class="intro-desc">
                <?=$group_description;?>
            </p>
          </div>
        </div>
      </div>
    </section>
    <!-- Group Info Slides Section End -->

    <!-- Gallary Section Start -->
    <section id="gallery" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Our Group Gallery</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Take a look of our greatest memories together</p>
            </div>
          </div> 
        </div>
        <div class="row">
          <div class="gallery_carousel owl-carousel">
          <?php
                $gallery_query = "SELECT * FROM gallery_tbl WHERE group_id='".$group_id."' AND is_featured = 1 
                    ORDER BY date_uploaded DESC, photo_id DESC";
                $gallery_fetch_result = mysqli_query($con, $gallery_query);
                while($row = mysqli_fetch_array($gallery_fetch_result)) {
                echo'
                  <div class="col-md-6 col-sm-6 col-lg-12">
                    <div class="gallery-box">
                      <div class="img-thumb">
                        <img class="img-fluid" src="_uploads/images/'.$row['photo'].'" alt="">
                      </div>
                      <div class="overlay-box text-center">
                        <a class="lightbox" href="_uploads/images/'.$row['photo'].'">
                          <i class="lni-plus"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                ';
              }
            ?>
          </div>
        </div>
      </div>
    </section>
    <!-- Gallary Section End -->

    <!-- Services Section Start -->
    <section id="services" class="services section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Our Achievements</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">We always make our University proud!</p>
            </div>
          </div>
        </div>
        <div class="row services-wrapper">
          <!-- Achievements item -->
            <?php
                $achievements_query = "SELECT * FROM achievements_tbl WHERE group_id='".$group_id."' 
                    ORDER BY achievement_year DESC";
                $achievements_fetch_result = mysqli_query($con, $achievements_query);
                while($row = mysqli_fetch_array($achievements_fetch_result)) {
                    echo'
                        <div class="col-md-6 col-lg-4 col-xs-12 padding-none">
                            <div class="services-item wow fadeInDown" data-wow-delay="0.2s">
                                <div class="icon">
                                    <i class="lni-cup"></i>
                                </div>
                                <div class="services-content">
                                    <h3><a href="#">'.$row['achievement_title'].'</a></h3>
                                    <p>'.$row['achievement_year'].'</p>
                                </div>
                            </div>
                        </div>
                    ';
                }
            ?>
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    <!-- Events Section Start -->
    <section id="events" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                <div class="section-title-header text-center">
                    <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Upcoming Events</h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s">Get updated with the upcoming events of the <?=$group_name;?></p>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="events_carousel owl-carousel">           
                <?php
                    $event_query = "SELECT * FROM events_tbl WHERE group_id='".$group_id."'";
                    $event_fetch_result = mysqli_query($con, $event_query);
                    while($row = mysqli_fetch_array($event_fetch_result)) {
                        $date = $row['event_date'];
                        $new_date = date('F d, Y', strtotime($date));
                        $event_title = $row['event_title'];
                        $event_title = (strlen($event_title) > 30) ? substr($event_title,0,30).'...' : $event_title;
                        echo '
                        <div class="col-lg-12 col-md-6 col-xs-12">  
                            <div class="blog-item">
                            <div class="blog-image">
                                <a href="event_single.php?event='.$row['event_id'].'">
                                <img class="img-fluid" src="_uploads/images/'.$row['event_picture'].'" alt="">
                                </a>
                            </div>
                            <div class="descr">
                                <div class="tag">'.$new_date.'</div>
                                <h3 class="title">
                                <a href="event_single.php?event='.$row['event_id'].'">
                                    '.$event_title.'
                                </a>
                                </h3>
                                <div class="meta-tags">
                                <span class="comments">
                                    <p>'.substr($row['event_description'],0,70).'...'.'</p>
                                </span>
                                </div>
                            </div>

                            <div class="w-space"></div>
                            <div class="view-txt">
                                <div class="Text-Style-10 "> 
                                <a class="btn btn-common btn-rm" href="event_single.php?event='.$row['event_id'].'">VIEW THIS EVENT</a> 
                                </div>
                            </div>
                            </div>
                        </div>
                        ';
                    }
                ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Schedule Section End -->

    <!-- Team Section Start -->
    <section id="team" class="section-padding text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Our Members</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Meet the <?=$group_name;?></p>
            </div>
          </div>
        </div>
        <div class="row">
          <?php
            $members_query = "SELECT * FROM members_tbl A 
              INNER JOIN student_record_tbl B ON A.student_record_id = B.student_record_id
              INNER JOIN courses_tbl C ON B.course_id = C.course_id
            WHERE A.group_id='".$group_id."'";
            $members_fetch_result = mysqli_query($con, $members_query);
            while($row = mysqli_fetch_array($members_fetch_result)) {
              echo'
              <div class="col-md-6 col-sm-6 col-lg-4">
                <!-- Team Item Starts -->
                <div class="team-item wow fadeInUp" data-wow-delay="0.2s">
                  <div class="team-img">
                    <img class="img-fluid" src="_uploads/images/'.$row['profile_picture'].'" alt="">
                  </div>
                  <div class="info-text">
                    <h3>'.$row['first_name'].' '.$row['last_name'].'</h3>
                    <p>Member, '.$row['course_acronym'].'</p>
                  </div>
                </div>
                <!-- Team Item Ends -->
              </div>
              ';
            }
          ?>
        </div>
      </div>
    </section>
    <!-- Team Section End -->
    
    <?php include 'footer.php'; ?>
    
    <div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info">
              <p>© Designed and Developed by <a href="http://uideck.com" rel="nofollow">UIdeck</a></p>
            </div>      
          </div>
        </div>
      </div>
    </div>

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
    </a>

    <div id="preloader">
      <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
      </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="_landing_assets/js/jquery-min.js"></script>
    <script src="_landing_assets/js/popper.min.js"></script>
    <script src="_landing_assets/js/bootstrap.min.js"></script>
    <script src="_landing_assets/js/jquery.countdown.min.js"></script>
    <script src="_landing_assets/js/jquery.nav.js"></script>
    <script src="_landing_assets/js/jquery.easing.min.js"></script>
    <script src="_landing_assets/js/wow.js"></script>
    <script src="_landing_assets/js/jquery.slicknav.js"></script>
    <script src="_landing_assets/js/nivo-lightbox.js"></script>
    <script src="_landing_assets/js/main.js"></script>
    <script src="_landing_assets/js/form-validator.min.js"></script>
    <script src="_landing_assets/js/contact-form-script.min.js"></script>
    <script src="_landing_assets/js/map.js"></script>
    <script src="_landing_assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      
  </body>
</html>
