<?php
require 'connect.php';

// Download document
if (!empty($_GET['document'])) {
  $document = mysqli_real_escape_string($con, $_GET['document']);
  $path = '_uploads/documents/'.$document.'';
  echo var_dump($path);
  if (file_exists($path)){
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($path).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    readfile($path);
    exit();
  }
}

// Download form
if (!empty($_GET['form'])) {
  $form = mysqli_real_escape_string($con, $_GET['form']);
  $path = '_uploads/documents/'.$form.'';
  echo var_dump($path);
  if (file_exists($path)){
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($path).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    readfile($path);
    exit();
  }
}
?>