<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-user-plus"></i> Add Student Record</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Student Records</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        <form class="form-horizontal" id="record_form" method="post" autocomplete="off">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Student Number</label>
                    <input type="text" name="student_number" id="student_number" class="form-control" placeholder="Enter student number" >
                    <span id='studnum_message'></span>
                  </div>
                  <!-- /.form-group -->

                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Middle Name (Optional)</label>
                    <input type="text" name="middle_name" id="middle_name" class="form-control" placeholder="Enter middle name" >
                    <span id='mname_message'></span>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Enter first name" >
                    <span id='fname_message'></span>
                  </div>
                  <!-- /.form-group -->

                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Enter last name" >
                    <span id='lname_message'></span>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Contact Number</label>
                      <input type="text" name="contact_number" id="contact_number" class="form-control" placeholder="Enter contact number" >
                    <span id='contact_message'></span>
                    <!-- /.input group -->
                  </div>
                  <!-- /.form group -->
                </div>
                <!-- /.col -->

                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label>Birthday</label>
                      <input type="text" name="birthday" id="birthday" placeholder="Enter birthday" class="form-control" data-toggle="datetimepicker" data-target="#birthday">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Sex</label>
                    <select name="sex" id="sex" class="form-control" style="width: 100%;">
                      <option value="1">Male</option>
                      <option value="2">Female</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Course</label>
                    <select name="course" id="course" class="form-control" style="width: 100%;">
                      <?php  
                          $course_query = "SELECT * FROM courses_tbl";
                          $course_result = mysqli_query($con, $course_query);
                          while($row = mysqli_fetch_array($course_result)) {
                              echo "<option value='".$row['course_id']."'>".$row['course_name']." (".$row['course_acronym'].")</option>";
                          }
                      ?>  
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-12">
                  <label>Present Address</label>
                  <textarea name="address" id="address" class="form-control" rows="3" placeholder="Enter present address"></textarea>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <a href="manage_records.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="add_student_record" id="add_student_record" value="Add Record" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
  $(function () {
    $('#birthday').datetimepicker({
      format: 'L'
    })
  })

//AJAX form submission with Validation
  $().ready(function() {    
    $("#record_form").validate({
      rules: {
        student_number:{
          required: true,
          number: true,
          minlength: 9,
          maxlength: 9,
          remote: {
            url: "admin_functions.php",
            type: "post",
            data: {
              validate_student_number: function() {
                return student_number;
              }
            }
          }
        },
        first_name: "required",
        last_name: "required",
        contact_number:{
          required: true,
          pattern: /^(09)\d{9}$/,
        },
        birthday: "required",
        address: "required"
      },
      messages: {
				student_number: {
          required: "Student number is required.",
          number: "Please enter a valid student number.",
          minlength: "Student number must be 9 numbers long.",
          maxlength: "Student number must be 9 numbers long.",
          remote: "Student number already exists."
        },
        first_name: "First name is required.",
        last_name: "Last name is required.",
        contact_number: {
          required: "Contact number is required.",
          pattern: "Contact number must start with 09 and 11 numbers long."
        },
        birthday: "Birthday is required.",
        address: "Address is required.",
			},
      submitHandler: function(form){
        var serializedData = $(form).serialize();
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to add this record?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: serializedData,
                  dataType: "json",
                  type: "POST",
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Student record successfully added!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_records.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>