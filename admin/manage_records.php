<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-users"></i> Manage Student Records</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Student Records</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_record.php">
                <i class="fas fa-plus"></i> Add Student Record
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Student Number</th>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $records_fetch_query = "SELECT * FROM student_record_tbl A 
                          INNER JOIN courses_tbl B ON A.course_id = B.course_id";
                        $records_fetch_result = mysqli_query($con, $records_fetch_query);
                        while($row = mysqli_fetch_array($records_fetch_result)) {
                      ?>
                        <tr>
                          <td><?=$row['student_number'];?></td>
                          <td><?=$row['first_name']." ".$row['last_name'];?></td>
                          <td><?=$row['course_name'];?> (<?=$row['course_acronym'];?>)</td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_record.php?record=<?= $row['student_record_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_record" id="<?=$row['student_record_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                      <th>Student Number</th>
                      <th>Name</th>
                      <th>Course</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Record------------------
  $(document).on('click', '.delete_record', function(){
      var delete_record = $(this).attr("id");
      var data = {      
        delete_record:delete_record
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this record?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Student record successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_records.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
</script>