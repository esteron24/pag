<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $form = $_GET['form'];
    $form_query = "SELECT * FROM forms_tbl WHERE form_id=".$form."";
    $form_result = mysqli_query($con, $form_query);
    $form_fetch = mysqli_fetch_assoc($form_result);
    $form_file = $form_fetch['form_file'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-file-alt"></i> View Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Downloadable Forms</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        
          <form class="form-horizontal" id="document_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">File Viewer</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="text-align:center;">
                <iframe src = "../ViewerJS/#../_uploads/documents/<?=$form_file;?>" width='800' height='600' allowfullscreen webkitallowfullscreen></iframe>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <a href="manage_forms.php" class="btn btn-secondary float-right">Cancel</a>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>