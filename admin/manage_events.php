<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-calendar-week"></i> Manage Events</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Events</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_event.php">
                <i class="fas fa-plus"></i> Add New Event
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Group</th>
                        <th>Event Title</th>
                        <th>Event Date</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $events_fetch_query = "SELECT * FROM events_tbl A 
                          INNER JOIN groups_tbl B ON A.group_id = B.group_id
                          ORDER BY A.event_date DESC";
                        $events_fetch_result = mysqli_query($con, $events_fetch_query);
                        while($row = mysqli_fetch_array($events_fetch_result)) {
                          $event_date = $row['event_date'];
                          $new_date = date('F d, Y', strtotime($event_date));
                          $event = $row['event_title'];
                          $event = (strlen($event) > 45) ? substr($event,0,44).'...' : $event;
                      ?>
                        <tr>
                          <td><?=$row['group_name'];?></td>
                          <td><?=$event;?></td>
                          <td><?=$new_date;?></td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_event.php?event=<?=$row['event_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_event" id="<?=$row['event_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                      <th>Group</th>
                      <th>Event Title</th>
                      <th>Event Date</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Event------------------
$(document).on('click', '.delete_event', function(){
      var delete_event = $(this).attr("id");
      var data = {      
        delete_event:delete_event
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this event?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Event successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_events.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
</script>