<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-download"></i> Manage Forms</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Downloadable Forms</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_form.php">
                <i class="fas fa-plus"></i> Add Form
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Group</th>
                        <th>Form</th>
                        <th>Date and Time Uploaded</th>
                        <th>Featured?</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $forms_fetch_query = "SELECT * FROM forms_tbl A LEFT JOIN groups_tbl B ON A.group_id = B.group_id";
                        $forms_fetch_result = mysqli_query($con, $forms_fetch_query);
                        while($row = mysqli_fetch_array($forms_fetch_result)) {
                          $datetime_uploaded = $row['date_uploaded'];
                          $date_uploaded = date('F d, Y', strtotime($datetime_uploaded));
                          $time_uploaded = date('h:i A', strtotime($datetime_uploaded));
                          $form = $row['form_name'];
                          $form = (strlen($form) > 45) ? substr($form,0,44).'...' : $form;        
                      ?>
                        <tr>
                          <?php
                            if($row['group_id'] == 0){
                              echo '<td>N/A</td>';
                            }else{
                              echo '<td>'.$row['group_name'].'</td>';
                            }
                          ?>
                          <td><?=$form;?></td>
                          <td>
                            <strong>Date:</strong> <?=$date_uploaded;?><br> 
                            <strong>Time:</strong> <?=$time_uploaded;?>
                          </td>
                          <td>
                            <?php
                              if($row['group_id'] ==! 0){
                                echo'<select name="is_featured" id="'.$row['form_id'].'" class="form-control is_featured" style="width: 100%;">';
                                        if($row['is_featured'] == 0){
                                          echo'
                                            <option value="1">Yes</option>
                                            <option selected value="0">No</option>
                                          ';
                                        }else{
                                          echo'
                                            <option selected value="1">Yes</option>
                                            <option value="0">No</option>
                                          ';
                                        }
                                echo'</select>';
                              }else{
                                  echo'N/A';
                              }
                            ?>
                          </td>
                          <td>
                            <a class="btn btn-warning btn-sm" href="view_form.php?form=<?=$row['form_id'];?>">
                              <i class="fas fa-eye"></i> View
                            </a>
                            <a class="btn btn-success btn-sm download_document" href="update_form.php?form=<?=$row['form_id'];?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <!-- <a class="btn btn-success btn-sm download_document" href="../download.php?document=">
                              <i class="fas fa-download"></i> Download
                            </a> -->
                            <a class="btn btn-danger btn-sm delete_form" href="#" id="<?=$row['form_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                            
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
            
                  <tfoot>
                    <tr>
                      <th>Group</th>
                      <th>Form</th>
                      <th>Date and Time Uploaded</th>
                      <th>Featured?</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Form------------------
  $(document).on('click', '.delete_form', function(){
      var delete_form = $(this).attr("id");
      var data = {      
        delete_form:delete_form
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this form?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Form successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_forms.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });

  //-------------------Set feature/unfeature file------------------
    $(document).on('change', '.is_featured', function(){
      var form_id = $(this).attr("id");
      var feature_form = $(this).val();
      var data = {
        form_id:form_id,     
        feature_form:feature_form
      };
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "admin_functions.php",
        dataType: "json",
        data: data,
        success: function(response){
          if(response == true){
            console.log("Success");
          }
        }
      });
    });
</script>