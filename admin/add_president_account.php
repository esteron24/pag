<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-user-plus"></i> Add President Account</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Accounts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<?php
    $student_record = $_GET['student'];
    $record_fetch_query = "SELECT student_record_id, CONCAT(first_name,' ',last_name) AS full_name FROM student_record_tbl WHERE student_number= ".$student_record."";
    $record_result = mysqli_query($con, $record_fetch_query); 
    $record_fetch = mysqli_fetch_assoc($record_result);

    $student_record_id = $record_fetch['student_record_id'];
    $name = $record_fetch['full_name'];
?>
    <!-- Main content -->
    <section class="content">

    <ul id="form-errors"></ul>

    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" method="post" id="president_form" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

            <div class="row">
                <div class="col-12 col-sm-6">
                 <!-- /.form-group -->
                 <div class="form-group">
                    <!-- Uploaded image area-->
                    <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                    <div class="profile-image" id="profile-image">
                        <img id="imageResult" src="#" alt=""> 
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>

            <div class="row">
              <div class="col-md-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Profile Picture</label>
                      <div class="custom-file">
                        <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                        <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                      </div>
                </div>
                  <!-- /.form-group -->  
              </div>
              <!-- /.col --> 

              <div class="col-md-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Group Affiliated</label>
                  <select name="group" id="group" class="form-control" style="width: 100%;">
                    <?php  
                      //   SELECT Id, ProductName, UnitPrice
                      //   FROM Product
                      //  WHERE UnitPrice NOT IN (10,20,30,40,50)
                        $group_query = "SELECT * FROM groups_tbl";
                        $group_result = mysqli_query($con, $group_query);
                        while($row = mysqli_fetch_array($group_result)) {
                            echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                        }
                    ?> 
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->  
                
            <div class="row">

              <div class="col-md-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="full_name" id="full_name" value="<?=$name;?>" class="form-control" disabled>
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" id="password" class="form-control" placeholder="Enter password">
                  <span id='pass_message'></span>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="username" id="username" class="form-control" placeholder="Enter username">
                  <span id='availability'></span>
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label>Re-type Password</label>
                  <input type="password" name="re_password" id="re_password" class="form-control" placeholder="Re-type password">
                  <span id='repass_message'></span>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->


            </div>
            <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <input type="hidden" name="record" id="record" value="<?=$student_record_id;?>">
                      <a href="manage_president_accounts.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="add_president_account" id="add_president_account" value="Add Account" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
$(document).ready(function () {
  bsCustomFileInput.init();
});

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#imageResult')
              .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(this);
    });
  });

//AJAX form submission with Validation
  $().ready(function() {
    $("#president_form").validate({
      rules:{
        upload: "required",
        username:{
          required: true,
          minlength: 5,
          remote: {
            url: "admin_functions.php",
            type: "post",
            data: {
              validate_username: function() {
                return username;
              }
            }
          }
        },
        group:{
          remote: {
            url: "admin_functions.php",
            type: "post",
            data: {
              group_availability: function() {
                return group;
              }
            }
          }
        },
        password:{
          required: true,
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/
        },
        re_password:{
          required: true,
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
          equalTo: "#password"
         
        },
        fb_account:{
          pattern: /(?:(?:http|https):\/\/)?(?:www.)?(?:facebook.com)\/([A-Za-z0-9-_\.]+)/im
        },
        ig_account:{
          pattern: /(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com)\/([A-Za-z0-9-_\.]+)/im
        },
        tw_account:{
          pattern: /(?:(?:http|https):\/\/)?(?:www.)?(?:twitter.com)\/([A-Za-z0-9-_\.]+)/im
        },
      },
      messages:{
        upload: "Profile picture is required.",
        username:{
          required: "Username is required.",
          minlength: "Username must be at least 5 characters long.",
          remote: "Username already exists."
        },
        password: {
          required: "Password is required.",
          pattern: "Password must be minimum of eight characters, at least one uppercase letter, one lowercase letter and one number.",
        },
        re_password: {
          required: "Password is required.",
          pattern: "Password must be minimum of eight characters, at least one uppercase letter, one lowercase letter and one number.",
          equalTo: "Password does not match."
        },
        fb_account:{
          pattern: "Invalid Facebook URL."
        },
        ig_account:{
          pattern: "Invalid Instagram URL."
        },
        tw_account:{
          pattern: "Invalid Twitter URL."
        },
      },
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        
        var formData = new FormData(form);

        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to add this account?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Account successfully added!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_president_accounts.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>