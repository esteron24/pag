<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-user-shield"></i> Manage Administrator Accounts</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Accounts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_admin_account.php">
                <i class="fas fa-plus"></i> Add Account
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Username</th>
                        <th>Date Registered</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $user = $_SESSION['session_admin_id'];
                        $accounts_fetch_query = "SELECT * FROM account_tbl A 
                          INNER JOIN access_level_tbl B ON A.access_level_id = B.access_level_id
                          WHERE A.access_level_id = 1";
                        $accounts_fetch_result = mysqli_query($con, $accounts_fetch_query);
                        while($row = mysqli_fetch_array($accounts_fetch_result)) {
                      ?>
                        <tr>
                          <td><?=$row['username'];?></td>
                          <td><?=$row['registration_date'];?></td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_admin_account.php?account=<?= $row['account_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                      <?php
                        if($user <> $row['account_id']){
                          echo 
                          '<button class="btn btn-danger btn-sm delete_account" id="'.$row['account_id'].'">
                            <i class="fas fa-trash"></i> Delete
                          </button>';
                        }
                      ?>  
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                      <th>Username</th>
                      <th>Date Registered</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });

    //-------------------Delete Account------------------
    $(document).on('click', '.delete_account', function(){
      var delete_admin_account = $(this).attr("id");
      var data = {      
        delete_admin_account:delete_admin_account
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this account?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Account successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_admin_accounts.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
  });


</script>