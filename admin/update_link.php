<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $link = $_GET['link'];
    $link_fetch_query = "SELECT * FROM other_link_tbl A 
      LEFT JOIN groups_tbl B ON A.group_id = B.group_id 
      WHERE A.link_id= ".$link."";
    $link_result = mysqli_query($con, $link_fetch_query); 
    $link_fetch = mysqli_fetch_assoc($link_result);

    $link_id = $link_fetch['link_id'];
    $link_name = $link_fetch['link_name'];
    $link_url = $link_fetch['link_url'];
    $group = $link_fetch['group_id'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-link"></i> Update Link</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Links</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        
          <form class="form-horizontal" id="link_form" method="post" autocomplete="off">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <!-- /.form-group -->
              <div class="form-group">
                  <label>Link Identifier</label>
                  <select name="link_identifier" id="link_identifier" onchange="formSelectCheck(this);" class="form-control" style="width: 100%;">
                    <option value="1">Coordinator</option>
                    <option value="2">President</option>
                  </select>
              </div>
              <!-- /.form-group -->

              <!-- /.form-group -->
              <div class="form-group" id="group_div" style="display:none;">
                  <label>Group</label>
                  <select name="group" id="group" class="form-control" style="width: 100%;">
                    <?php  
                      $group_query = "SELECT * FROM groups_tbl";
                      $group_result = mysqli_query($con, $group_query);
                      while($row = mysqli_fetch_array($group_result)) {
                          echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                      }
                    ?> 
                  </select>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label>Link Name</label>
                <input type="text" name="link_name" id="link_name" value="<?=$link_name;?>" class="form-control" placeholder="Enter link name">
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label>Link URL</label>
                <input type="text" name="link_url" id="link_url" value="<?=$link_url;?>" class="form-control" placeholder="Enter link url">
              </div>
              <!-- /.form-group -->            
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <input type="hidden" name="link_id" id="link_id" value="<?=$link_id;?>">
                      <a href="manage_links.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="update_link" id="update_link" value="Update Link" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>

    function formSelectCheck(type){
        if(type){
        var selection = $("#link_identifier").val();
        if(selection == 2){
            document.getElementById("group_div").style.display = "block";
        }else{
            document.getElementById("group_div").style.display = "none";
        }
        }else{
        document.getElementById("group_div").style.display = "none";
        }
    }

//AJAX form submission with Validation
  $().ready(function() {  
    //JS set selected element in dropdown
    var group = "<?php echo $group ?>";
    if(group != 0){
        $("#link_identifier").val(2).change();
        $("#group").val(group).change(); 
    }else{
        $("#link_identifier").val(1).change();
    } 

    $("#link_form").validate({
      rules: {
        link_name: "required",
        link_url: "required"
      },
      messages: {
        link_name: {
          required: "Link name is required.",
        },
        link_url: {
          required: "Link URL is required."
        } 
      },
      submitHandler: function(form){
        // var formData = new FormData(form);
        var serializedData = $(form).serialize();
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update this link?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: serializedData,
                  dataType: "json",
                  type: "POST",
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Link successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_links.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>