<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-file-alt"></i> Add Document</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Documents</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        
          <form class="form-horizontal" id="document_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group">
                <label>Name of Document</label>
                <input type="text" name="document_name" id="document_name" class="form-control" placeholder="Enter document name">
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label>Document (Accepts .pdf files only)</label>
                <input type="file" name="document" id="document" class="form-control" placeholder="Enter document files">
              </div>
              <!-- /.form-group -->

              <!-- /.form-group -->
              <div class="form-group">
                  <label>Group</label>
                  <select name="group" id="group" class="form-control" style="width: 100%;">
                    <?php  
                      $group_query = "SELECT * FROM groups_tbl";
                      $group_result = mysqli_query($con, $group_query);
                      while($row = mysqli_fetch_array($group_result)) {
                          echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                      }
                    ?> 
                  </select>
              </div>
              <!-- /.form-group -->
              
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <a href="manage_documents.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="add_document" id="add_document" value="Add Document" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
  $(function () {
    $('#event_datetime_end').datetimepicker({
      // format: 'Y',
    })
  })

//AJAX form submission with Validation
  $().ready(function() {    
    $("#document_form").validate({
      rules: {
        document:{
          required: true,
          extension: "pdf"
        },
        document_name: "required"
      },
      messages: {
		    document: {
          required: "Please insert a document.",
          extension: "Select a valid input file format.",
        },
        document_name: {
          required: "Document name is required."
        } 
			},
      submitHandler: function(form){
        var formData = new FormData(form);
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to add this document?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Document successfully added!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_documents.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>