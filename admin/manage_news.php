<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-newspaper"></i> Manage News</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">News</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_news.php">
                <i class="fas fa-plus"></i> Add News
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Group Posted</th>
                        <th>News Title</th>
                        <th>Date Created</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $news_fetch_query = "SELECT * FROM news_tbl A 
                          INNER JOIN groups_tbl B ON A.group_id = B.group_id";
                        $news_fetch_result = mysqli_query($con, $news_fetch_query);
                        while($row = mysqli_fetch_array($news_fetch_result)) {
                          $news_date = $row['news_date_created'];
                          $new_date = date('F d, Y', strtotime($news_date));
                          $news = $row['news_title'];
                          $news = (strlen($news) > 45) ? substr($news,0,44).'...' : $news;
                      ?>
                        <tr>
                          <td><?=$row['group_name'];?></td> 
                          <td><?=$news;?></td>
                          <td><?=$new_date;?></td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_news.php?news=<?= $row['news_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_news" id="<?=$row['news_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                      <th>Group Posted</th>
                      <th>News Title</th>
                      <th>Date Created</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Event------------------
  $(document).on('click', '.delete_news', function(){
      var delete_news = $(this).attr("id");
      var data = {      
        delete_news:delete_news
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this news?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'News successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_news.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
</script>