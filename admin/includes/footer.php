  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.1
    </div>
    <strong>Copyright &copy; <?=date("Y");?> <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

<!-- jQuery -->
<script src="../_assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery Validate-->
<script src="../_assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<!-- jQuery Additonal Methods-->
<script src="../_assets/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../_assets/plugins/select2/js/select2.full.min.js"></script>
<!-- Toastr -->
<script src="../_assets/plugins/toastr/toastr.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../_assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../_assets/plugins/moment/moment.min.js"></script>
<script src="../_assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="../_assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- DataTables -->
<script src="../_assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="../_assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- bootstrap color picker -->
<script src="../_assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../_assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="../_assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- bs-custom-file-input -->
<script src="../_assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- Jquery Confirm -->
<script src="../_assets/plugins/jquery-confirm/jquery-confirm.min.js"></script>
<!-- AdminLTE App -->
<script src="../_assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../_assets/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../_assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- CKEditor -->
<!-- <script src="../_assets/ckeditor/ckeditor.js"></script> -->
<!-- TinyMCE -->
<script src="../_assets/tinymce/tinymce.min.js"></script>
</body>
</html>
