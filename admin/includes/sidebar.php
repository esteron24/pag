<?php
  $admin_id = $_SESSION['session_admin_id'];

  $account_query = "SELECT CONCAT(B.first_name,' ',B.last_name) AS full_name 
    FROM account_tbl A INNER JOIN student_record_tbl B ON A.student_record_id = B.student_record_id 
    WHERE A.account_id = ".$admin_id."";

  $account_result = mysqli_query($con, $account_query);
  $account_fetch = mysqli_fetch_assoc($account_result);
  $full_name = $account_fetch['full_name'];
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../_assets/dist/img/cvsu.png" alt="PAG Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">PAG | Coordinator</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../_assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?=$full_name;?></a>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="index.php" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
          </a>
        </li>

        <li class="nav-header">USER SETTINGS</li>
        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-users-cog"></i>
              <p>Accounts <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="manage_president_accounts.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>President Account</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="manage_admin_accounts.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrator Account</p>
                </a>
              </li>
            </ul>
          </li>

        <li class="nav-item">
          <a href="manage_records.php" class="nav-link">
          <i class="nav-icon fas fa-address-book"></i> 
          <p>Student Records</p>
          </a>
        </li>

        <li class="nav-header">ADMINISTRATIVE SETTINGS</li>
        <li class="nav-item">
          <a href="manage_documents.php" class="nav-link">
            <i class="nav-icon fas fa-file-alt"></i>
            <p> Documents</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_forms.php" class="nav-link">
            <i class="nav-icon fas fa-download"></i>
            <p> Downloadable Forms</p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="manage_groups.php" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>Groups</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_members.php" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>Members</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_events.php" class="nav-link">
            <i class="nav-icon fas fa-calendar-week"></i>
            <p>Events</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_news.php" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>News</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_achievements.php" class="nav-link">
            <i class="nav-icon fas fa-trophy"></i>
            <p> Achievements</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_gallery.php" class="nav-link">
            <i class="nav-icon far fa-image"></i>
            <p>Gallery</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="manage_links.php" class="nav-link">
            <i class="nav-icon fas fa-link"></i>
            <p>Footer Links</p>
          </a>
        </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>