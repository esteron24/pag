<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-users"></i> Manage Groups</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Groups</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_group.php">
                <i class="fas fa-plus"></i> Add New Group
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Group Name</th>
                        <th>Group Created</th>
                        <th>Group Updated</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $group_fetch_query = "SELECT * FROM groups_tbl";
                        $group_fetch_result = mysqli_query($con, $group_fetch_query);
                        while($row = mysqli_fetch_array($group_fetch_result)) {
                          $datetime_created = $row['group_created'];
                          $date_created = date('F d, Y', strtotime($datetime_created));
                          $time_created = date('h:i A', strtotime($datetime_created));

                          $datetime_updated = $row['group_updated'];
                          $date_updated = date('F d, Y', strtotime($datetime_updated));
                          $time_updated = date('h:i A', strtotime($datetime_updated));
                      ?>
                        <tr>
                          <td><?=$row['group_name'];?></td>
                          <td>
                            <strong>Date:</strong> <?=$date_created;?><br>
                            <strong>Time:</strong> <?=$time_created;?>
                          </td>
                          <?php
                            if($datetime_created == $datetime_updated){
                              echo '<td>N/A</td>';
                            }else{
                              echo '
                              <td>
                                <strong>Date:</strong> '.$date_updated.'<br>
                                <strong>Time:</strong> '.$time_updated.'
                              </td>';
                            }
                          ?>
                          
                          <td>
                            <a class="btn btn-success btn-sm" href="update_group.php?group=<?= $row['group_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <button class="btn btn-danger btn-sm delete_group" id="<?=$row['group_id'];?>">
                              <i class="fas fa-trash"></i> Delete
                            </button>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                      <th>Group Name</th>
                      <th>Group Created</th>
                      <th>Group Updated</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Group------------------
$(document).on('click', '.delete_group', function(){
      var delete_group = $(this).attr("id");
      var data = {      
        delete_group:delete_group
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this group?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Group successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_groups.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
</script>