<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Coordinator Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <?php
                $count_groups_query = "SELECT COUNT(*) AS groups FROM groups_tbl";
                $group_result = mysqli_query($con, $count_groups_query);
                $group_fetch = mysqli_fetch_assoc($group_result);
                $total_group = $group_fetch['groups'];
              ?>
                <h3><?=$total_group;?></h3>
                <p>Total Groups</p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <a href="manage_groups.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <?php
                $count_president_query = "SELECT COUNT(*) AS president FROM account_tbl WHERE access_level_id = 2";
                $president_result = mysqli_query($con, $count_president_query);
                $president_fetch = mysqli_fetch_assoc($president_result);
                $total_president = $president_fetch['president'];
              ?>
                <h3><?=$total_president;?></h3>
                <p>Total President Accounts</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="manage_president_accounts.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <?php
                $count_documents_query = "SELECT COUNT(*) AS documents FROM documents_tbl";
                $documents_result = mysqli_query($con, $count_documents_query);
                $documents_fetch = mysqli_fetch_assoc($documents_result);
                $total_documents = $documents_fetch['documents'];
              ?>
                <h3><?=$total_documents;?></h3>
                <p>Documents Received</p>
              </div>
              <div class="icon">
                <i class="fas fa-file-alt"></i>
              </div>
              <a href="manage_documents.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
         
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              <?php
                $count_forms_query = "SELECT COUNT(*) AS forms FROM forms_tbl";
                $forms_result = mysqli_query($con, $count_forms_query);
                $forms_fetch = mysqli_fetch_assoc($forms_result);
                $total_forms = $forms_fetch['forms'];
              ?>
                <h3><?=$total_forms;?></h3>
                <p>Forms Uploaded</p>
              </div>
              <div class="icon">
                <i class="fas fa-download"></i>
              </div>
              <a href="manage_forms.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>