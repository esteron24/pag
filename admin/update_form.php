<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $form = $_GET['form'];
    $form_fetch_query = "SELECT * FROM forms_tbl A 
      LEFT JOIN groups_tbl B ON A.group_id = B.group_id 
      WHERE A.form_id= ".$form."";
    $form_result = mysqli_query($con, $form_fetch_query); 
    $form_fetch = mysqli_fetch_assoc($form_result);

    $form_id = $form_fetch['form_id'];
    $form_name = $form_fetch['form_name'];
    $form_type = $form_fetch['form_type_id'];
    $group = $form_fetch['group_id'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-file-alt"></i> Update Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Downloadable Forms</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        
          <form class="form-horizontal" id="document_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="form-group">
                    <label>Name of Form</label>
                    <input type="text" name="document_name" id="document_name" value="<?=$form_name;?>" class="form-control" placeholder="Enter document name">
                </div>
                <!-- /.form-group -->
                              
                <div class="form-group">
                    <label>Form (Accepts .pdf files only)</label>
                    <input type="file" name="document" id="document" class="form-control" placeholder="Enter document files">
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                    <label>Form Type</label>
                    <select name="form_type" id="form_type" onchange="formSelectCheck(this);" class="form-control" style="width: 100%;">
                        <option value="1">Coordinator Form</option>
                        <option value="2">President Form</option>
                    </select>
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group" id="group_div" style="display:none;">
                    <label>Group</label>
                    <select name="group" id="group" class="form-control" style="width: 100%;">
                        <?php  
                        $group_query = "SELECT * FROM groups_tbl";
                        $group_result = mysqli_query($con, $group_query);
                        while($row = mysqli_fetch_array($group_result)) {
                            echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                        }
                        ?> 
                    </select>
                </div>
                <!-- /.form-group -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <input type="hidden" name="form" id="form" value="<?=$form_id;?>">
                      <a href="manage_forms.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="update_form" id="update_form" value="Update Form" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
  $(function () {
    $('#event_datetime_end').datetimepicker({
      // format: 'Y',
    })
  })

  function formSelectCheck(type){
    if(type){
      var selection = $("#form_type").val();
      if(selection == 2){
        document.getElementById("group_div").style.display = "block";
      }else{
        document.getElementById("group_div").style.display = "none";
      }
    }else{
      document.getElementById("group_div").style.display = "none";
    }
  }

//AJAX form submission with Validation
  $().ready(function() {
    //JS set selected element in dropdown
    var form_type = "<?php echo $form_type ?>";
    $("#form_type").val(form_type).change();

    if(form_type == 2){
        var group = "<?php echo $group ?>";
        $("#group").val(group).change();
    }
    
    $("#document_form").validate({
      rules: {
        document:{
          extension: "pdf"
        },
        document_name: "required"
      },
      messages: {
        document: {
          extension: "Select a valid input file format.",
        },
        document_name: {
          required: "Form name is required."
        } 
			},
      submitHandler: function(form){
        var formData = new FormData(form);
        var selection = $("#form_type").val();
        var group = $("#group").val();
        if(selection == 2){
          formData.append('group', group);
        }else{
          formData.delete('group');
        }
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update this form?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Form successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_forms.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>