<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $member = $_GET['member'];
    $member_fetch_query = "SELECT * FROM student_record_tbl A 
      INNER JOIN courses_tbl B ON A.course_id = B.course_id 
      INNER JOIN members_tbl C ON A.student_record_id = C.student_record_id
      WHERE C.member_id= ".$member."";
    $member_result = mysqli_query($con, $member_fetch_query); 
    $member_fetch = mysqli_fetch_assoc($member_result);

    $first_name = $member_fetch['first_name'];
    $last_name = $member_fetch['last_name'];
    $student_number = $member_fetch['student_number'];
    $course = $member_fetch['course_name'];
    $course_acronym = $member_fetch['course_acronym'];
    $group_id = $member_fetch['group_id'];
    $profile_picture = $member_fetch['profile_picture'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-user"></i> Update Member</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Membership</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

    <ul id="form-errors"></ul>

    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" method="post" id="member_form" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <div class="row">
                  <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                      <!-- Uploaded image area-->
                      <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                      <div class="profile-image" id="profile-image">
                          <img id="imageResult" src="../_uploads/images/<?=$profile_picture;?>" alt=""> 
                      </div>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
              </div>

              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Profile Picture</label>
                        <div class="custom-file">
                          <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                          <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                        </div>
                  </div>
                    <!-- /.form-group -->  
                </div>
                <!-- /.col -->
              </div> 

              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Group Affiliated</label>
                    <select name="group" id="group" class="form-control" style="width: 100%;">
                      <?php  
                            $group_query = "SELECT * FROM groups_tbl";
                            $group_result = mysqli_query($con, $group_query);
                            while($row = mysqli_fetch_array($group_result)) {
                                echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                            }
                        ?> 
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Student Number</label>
                    <input type="text" name="student_number" id="student_number" value="<?=$student_number;?>" class="form-control" disabled>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>

              <div class="row">
               <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="full_name" id="full_name" value="<?=$first_name;?> <?=$last_name;?>" class="form-control" disabled>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Course</label>
                    <input type="text" name="course" id="course" value="<?=$course;?> (<?=$course_acronym;?>)" class="form-control" disabled>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <input type="hidden" name="member" id="member" value="<?=$member;?>">
                      <a href="manage_members.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="update_member" id="update_member" value="Update Member" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
  /*  ==========================================
      SHOW UPLOADED IMAGE
  * ========================================== */
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#imageResult')
              .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(this);
    });
  });

  $().ready(function() {
  //JS set selected element in dropdown
    var db_group = "<?php echo $group_id ?>";
    $("#group").val(db_group).change();
  });

//AJAX submit form
  // $(document).on('submit', function(){
  //   var member = $('#member').val();
  //   var group = $('#group').val();
  //   var data = {
  //     update_member: "update_member",      
  //     member:member,
  //     group:group
  //   };
  //   event.preventDefault();
  //   $.confirm({
  //     icon: 'fas fa-exclamation-triangle',
  //     title: 'Attention',
  //     content: 'Are you sure you want to update this membership?',
  //     type: 'orange',
  //     buttons: {
  //       confirm: {
  //         closeIcon: true,
  //         btnClass: 'btn-orange',
  //         action: function(){
  //           $.ajax({
  //             url: "admin_functions.php",
  //             data: data,
  //             dataType: "json",
  //             type: "POST",
  //             processData: false,
  //             contentType: false,
  //             success: function(response){
  //               if(response == true){
  //                 $.confirm({
  //                   icon: 'fas fa-check',
  //                   title: 'Success',
  //                   content: 'Membership successfully updated!',
  //                   type: 'green',
  //                   typeAnimated: true,
  //                   buttons: {
  //                       close: function () {
  //                         window.location.href="manage_members.php";
  //                       }
  //                   }
  //                 });
  //               }
  //             }
  //           });
  //         }
  //       },
  //       cancel: function () {
  //         //Cancel AJAX Request
  //       }
  //     }
  //   });
  // });

  $().ready(function() {
    $("#member_form").validate({
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        var formData = new FormData(form);
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update this membership?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Membership successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_members.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>