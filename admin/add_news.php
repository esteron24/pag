<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-newspaper"></i> Create News</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">News</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="container-fluid">
          <div class="card card-success">
            <form class="form-horizontal" id="news_form" method="post" autocomplete="off" enctype="multipart/form-data">
              <div class="card-header">
                <h3 class="card-title">Please fill up the fields accurately.</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">

                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="form-group">
                      <!-- Uploaded image area-->
                      <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                      <div class="dimage" id="event-image">
                        <img id="imageResult" src="#" alt=" "> 
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Group</label>
                      <select name="group" id="group" class="form-control" style="width: 100%;">
                        <?php  
                              $group_query = "SELECT * FROM groups_tbl";
                              $group_result = mysqli_query($con, $group_query);
                              while($row = mysqli_fetch_array($group_result)) {
                                  echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                              }
                          ?> 
                      </select>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-lg-6">
                    <!-- /.form-group -->
                    <div class="form-group">
                      <label>News Photo</label>
                        <div class="custom-file">
                        <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                          <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                        </div>
                    </div>
                    <!-- /.form-group -->
                  </div>

                  <div class="col-lg-6">
                    <!-- /.form-group -->
                    <div class="form-group">
                      <label>News Title</label>
                      <input type="text" name="news_title" id="news_title" placeholder="Enter news title" value="" class="form-control">
                    </div>
                    <!-- /.form-group -->
                  </div>
                </div>
                            
                <div class="row">
                  <div class="col-lg-12">
                    <div class="description_item">
                      <label>News Content</label>
                      <textarea name="news_content" id="news_content" placeholder="Enter news content" class="form-control"></textarea>
                    </div>
                  </div>
                </div>

              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="row">
                  <div class="col-12">
                    <a href="manage_news.php" class="btn btn-secondary float-right">Cancel</a>
                    <input type="submit" name="add_news" id="add_news" value="Add News" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div><!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
// CKEDITOR.replace('news_content');

  $(document).ready(function () {
    bsCustomFileInput.init();
  });

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(this);
    });
  });

//Initialize WYSIWYG to textarea using TinyMCE
  // tinymce.init({
  //   selector: '#news_content',
  //   height: '400',
  //   onchange_callback: function(editor) {
	// 		tinyMCE.triggerSave();
	// 		$("#" + editor.id).valid();
	// 	}
  // });

//AJAX form submission with Validation
  $().ready(function() {
    $("#news_form").validate({
    // var validator = $("#news_form").submit(function() {
		// 	// update underlying textarea before submit validation
		// 	tinyMCE.triggerSave();
		// }).validate({
    //   ignore: "",
      rules:{
        upload: "required",
        news_title:{
          required: true,
          minlength: 10
        },
        news_content:{
          required: true,
          minlength: 150
        },
      },
      messages:{
        upload: "Image is required.",
        news_title:{
          required: "News title is required.",
          minlength: "News title must be at least 10 characters long.",
        },
        news_content:{
          required: "News content is required.",
          minlength: "News content must be at least 150 characters long.",
        }
      },
      // errorPlacement: function(label, element) {
			// 	// position error label after generated textarea
			// 	if (element.is("textarea")) {
			// 		label.insertAfter(element.next());
			// 	} else {
			// 		label.insertAfter(element)
			// 	}
			// },
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        var formData = new FormData(form);
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to add this news?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'News successfully added!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_news.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });

    // validator.focusInvalid = function() {
		// 	// put focus on tinymce on submit validation
		// 	if (this.settings.focusInvalid) {
		// 		try {
		// 			var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
		// 			if (toFocus.is("textarea")) {
		// 				tinyMCE.get(toFocus.attr("id")).focus();
		// 			} else {
		// 				toFocus.filter(":visible").focus();
		// 			}
		// 		} catch (e) {
		// 			// ignore IE throwing errors when focusing hidden elements
		// 		}
		// 	}
		// }
  });
</script>