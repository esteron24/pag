<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $group = $_GET['group'];
    $group_query = "SELECT * FROM groups_tbl WHERE group_id =".$group."";

    $group_result = mysqli_query($con, $group_query);
    $group_fetch = mysqli_fetch_assoc($group_result);
    $group_logo = $group_fetch['group_logo'];
    $group_name = $group_fetch['group_name'];
    $embed_link = $group_fetch['video_embed_link'];
    $group_description = $group_fetch['group_description'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-edit"></i> Update Group</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Groups</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" id="group_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                 <!-- /.form-group -->
                 <div class="form-group">
                      <!-- Uploaded image area-->
                      <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                      <div class="profile-image" id="profile-image">
                          <img id="imageResult" src="../_uploads/logos/<?=$group_logo;?>" alt=" "> 
                      </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Group Logo</label>
                      <div class="custom-file">
                        <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                        <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                      </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Group Name</label>
                    <input type="text" name="group_name" id="group_name" value="<?=$group_name;?>" placeholder="Enter group name" class="form-control">
                    <span id='availability'></span>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Cover Image</label>
                      <div class="custom-file">
                      <input type="file" class="form-control" name="cover_upload" id="cover_upload" accept="image/*">
                        <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                      </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Faceboook/Youtube Video Embed Link</label>
                    <div class="custom-file">
                      <input type="text" class="form-control" name="embed_link" id="embed_link" value="<?=$embed_link;?>" placeholder="Enter video embed link">
                      <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <div class="col-lg-12">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <div class="description_item">
                      <label>Group Description</label>
                      <textarea name="description" id="description" class="form-control" rows="6" placeholder="Enter Group's Description"><?=$group_description;?></textarea>
                      <!-- <span id='description_message'></span> -->
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                    <input type="hidden" name="group" id="group" value="<?=$group;?>">
                    <a href="manage_groups.php" class="btn btn-secondary float-right">Cancel</a>
                    <input type="submit" name="update_group" id="update_group" value="Update Group" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
$(document).ready(function () {
  bsCustomFileInput.init();
});

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(this);
    });
  });

//Initialize WYSIWYG to textarea using TinyMCE
  // tinymce.init({
  //   selector: '#description',
  //   height: '400',
  //   onchange_callback: function(editor) {
	// 		tinyMCE.triggerSave();
	// 		$("#" + editor.id).valid();
	// 	}
  // });

//AJAX form submission with Validation
  $().ready(function() {
    var db_group = "<?php echo $group_name ?>";
    $("#group_form").validate({
    // var validator = $("#group_form").submit(function() {
		// 	// update underlying textarea before submit validation
		// 	tinyMCE.triggerSave();
		// }).validate({
    //   ignore: "",
      rules:{
        group_name:{
          required: true,
          minlength: 5,
          remote: {
            url: "admin_functions.php",
            type: "post",
            data: {
              update_group_name: function() {
                return group_name;
              },
              db_group: function() {
                return db_group;
              },
            }
          }
        },
        embed_link: "required",
        description:{
          required: true,
          minlength: 150
        }
      },
      messages:{
        group_name:{
          required: "Group name is required.",
          minlength: "Group name must be at least 5 characters long.",
          remote: "Group name already exists."
        },
        description: {
          required: "Group's description is required.",
          minlength: "Group's description must be at least 150 characters long."
        },
        embed_link: "Facebook/Youtube video embed link is required."
      },
      // errorPlacement: function(label, element) {
			// 	// position error label after generated textarea
			// 	if (element.is("textarea")) {
			// 		label.insertAfter(element.next());
			// 	} else {
			// 		label.insertAfter(element)
			// 	}
			// },
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        var formData = new FormData(form);
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update this group?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Group successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_groups.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });

    // validator.focusInvalid = function() {
		// 	// put focus on tinymce on submit validation
		// 	if (this.settings.focusInvalid) {
		// 		try {
		// 			var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
		// 			if (toFocus.is("textarea")) {
		// 				tinyMCE.get(toFocus.attr("id")).focus();
		// 			} else {
		// 				toFocus.filter(":visible").focus();
		// 			}
		// 		} catch (e) {
		// 			// ignore IE throwing errors when focusing hidden elements
		// 		}
		// 	}
		// }
  });

</script>