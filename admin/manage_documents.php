<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-file-alt"></i> Manage Documents</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Documents</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_document.php">
                <i class="fas fa-plus"></i> Add Document
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Group Submitted</th>
                        <th>Document</th>
                        <th>Date and Time Received</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $documents_fetch_query = "SELECT * FROM documents_tbl A 
                          INNER JOIN groups_tbl B ON A.group_id = B.group_id ORDER BY A.group_id";
                        $documents_fetch_result = mysqli_query($con, $documents_fetch_query);
                        while($row = mysqli_fetch_array($documents_fetch_result)) {
                          $datetime_sent = $row['date_sent'];
                          $date_sent = date('F d, Y', strtotime($datetime_sent));
                          $time_sent = date('h:i A', strtotime($datetime_sent));
                          $document = $row['document_name'];
                          $document = (strlen($document) > 45) ? substr($document,0,44).'...' : $document;     
                      ?>
                        <tr>
                          <td><?=$row['group_name'];?></td>
                          <td><?=$document;?></td>
                          <td>
                            <strong>Date:</strong> <?=$date_sent;?><br> 
                            <strong>Time:</strong> <?=$time_sent;?>
                          </td>
                          <td>
                            <a class="btn btn-warning btn-sm" href="view_document.php?document=<?=$row['document_id'];?>">
                              <i class="fas fa-eye"></i> View
                            </a>
                            <a class="btn btn-success btn-sm download_document" href="update_document.php?document=<?=$row['document_id'];?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_document" href="#" id="<?=$row['document_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                            
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
            
                  <tfoot>
                    <tr>
                      <th>Group Submitted</th>
                      <th>Document</th>
                      <th>Date and Time Received</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Document------------------
  $(document).on('click', '.delete_document', function(){
      var delete_document = $(this).attr("id");
      var data = {      
        delete_document:delete_document
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this document?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "admin_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Document successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="manage_documents.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
</script>