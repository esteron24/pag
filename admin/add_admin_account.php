<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-user-plus"></i> Add Administrator Account</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Accounts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        <form class="form-horizontal" id="admin_form" method="post" autocomplete="off">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- /.form-group -->
                <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" id="username" class="form-control" placeholder="Enter username">
                <span id='availability'></span>
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Enter password">
                <span id='pass_message'></span>
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                <label>Re-type Password</label>
                <input type="password" name="re_password" id="re_password" class="form-control" placeholder="Re-type password">
                <span id='repass_message'></span>
                </div>
                <!-- /.form-group -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <a href="manage_admin_accounts.php" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="add_admin_account" id="add_admin_account" value="Add Account" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
            </form>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
//AJAX form submission with Validation
  $().ready(function() {
    $("#admin_form").validate({
      rules: {
        username:{
          required: true,
          minlength: 5,
          remote: {
            url: "admin_functions.php",
            type: "post",
            data: {
              validate_username: function() {
                return username;
              }
            }
          }
        },
        password:{
          required: true,
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/
        },
        re_password:{
          required: true,
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
          equalTo: "#password"
        }
      },
      messages: {
				username: {
          required: "Username is required.",
          minlength: "Username must be at least 5 characters long.",
          remote: "Username already exists."
        },
        password: {
          required: "Password is required.",
          pattern: "Password must be minimum of eight characters, at least one uppercase letter, one lowercase letter and one number.",
        },
        re_password: {
          required: "Password is required.",
          pattern: "Password must be minimum of eight characters, at least one uppercase letter, one lowercase letter and one number.",
          equalTo: "Password does not match."
        },
			},
      submitHandler: function(form){
        var serializedData = $(form).serialize();
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to add this account?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: serializedData,
                  dataType: "json",
                  type: "POST",
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Account successfully added!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_admin_accounts.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>



