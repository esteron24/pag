<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-calendar-week"></i> Create New Event</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Events</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" id="event_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <!-- Uploaded image area-->
                    <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                    <div class="dimage" id="event-image">
                      <img id="imageResult" src="#" alt=" "> 
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Group</label>
                    <select name="group" id="group" class="form-control" style="width: 100%;">
                      <?php  
                            $group_query = "SELECT * FROM groups_tbl";
                            $group_result = mysqli_query($con, $group_query);
                            while($row = mysqli_fetch_array($group_result)) {
                                echo "<option value='".$row['group_id']."'>".$row['group_name']."</option>";
                            }
                        ?> 
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>     

              <div class="row">
                <div class="col-lg-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Event Photo</label>
                      <div class="custom-file">
                      <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                        <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                      </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-lg-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Event Title</label>
                    <input type="text" name="event_title" id="event_title" placeholder="Enter event title" class="form-control">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Event Location</label>
                    <input type="text" name="event_location" id="event_location" placeholder="Enter event location" class="form-control">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Event Date</label>
                      <input type="text" name="event_date" id="event_date" placeholder="Enter event date" class="form-control" data-toggle="datetimepicker" data-target="#event_date">
                  </div>
                  <!-- /.form-group -->
                </div>
              </div>
              <!-- /.row -->

                <div class="row">
                  <div class="col-lg-12">
                    <div class="description_item">
                      <label>Event Description</label>
                      <textarea name="event_description" id="event_description" class="form-control" placeholder="Enter event description" rows="6"></textarea>
                    </div>
                  </div>
                </div>

              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <a href="manage_events.php" class="btn btn-secondary float-right">Cancel</a>
                        <input type="submit" name="add_event" id="add_event" value="Add Event" class="btn btn-success float-right" style="margin-right: 10px;"> 
                    </div>
                </div>
              </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'admin_functions.php';?>

<script>
  

$(function () {
//Initialize CKEditor Elements
  // CKEDITOR.replace('event_description');

//Initialize Select2 Elements
  $('.select2').select2()

  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  //Datemask2 mm/dd/yyyy
  $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
  //Money Euro
  $('[data-mask]').inputmask()

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({
  timePicker: true,
  timePickerIncrement: 30,
  locale: {
      format: 'MM/DD/YYYY hh:mm A'
  }
  })

  $('#event_date').datetimepicker({
    format: 'L',
    minDate: new Date()
  })
})

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
  });

//Initialize WYSIWYG to textarea using TinyMCE
  // tinymce.init({
  //   selector: '#event_description',
  //   height: '400',
  //   onchange_callback: function(editor) {
	// 		tinyMCE.triggerSave();
	// 		$("#" + editor.id).valid();
	// 	}
  // });

//AJAX form submission with Validation
  $().ready(function() {
    $("#event_form").validate({
    // var validator = $("#event_form").submit(function() {
		// 	// update underlying textarea before submit validation
		// 	tinyMCE.triggerSave();
		// }).validate({
      // ignore: "",
      rules:{
        upload: "required",
        event_title: "required",
        event_location: "required",
        event_date: "required",
        event_description:{
          required: true,
          minlength: 100,
        }
      },
      messages:{
        upload: "Image is required.",
        event_title: "Event title is required",
        event_location: "Event location is required",
        event_date: "Event date is required",
        event_description:{
          required: "Event description is required",
          minlength: "Event description must be at least 100 characters long",
        }
      },
      // errorPlacement: function(label, element) {
			// 	// position error label after generated textarea
			// 	if (element.is("textarea")) {
			// 		label.insertAfter(element.next());
			// 	} else {
			// 		label.insertAfter(element)
			// 	}
			// },
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        var formData = new FormData(form);
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to add this event?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "admin_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Event successfully added!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_events.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });

    // validator.focusInvalid = function() {
		// 	// put focus on tinymce on submit validation
		// 	if (this.settings.focusInvalid) {
		// 		try {
		// 			var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
		// 			if (toFocus.is("textarea")) {
		// 				tinyMCE.get(toFocus.attr("id")).focus();
		// 			} else {
		// 				toFocus.filter(":visible").focus();
		// 			}
		// 		} catch (e) {
		// 			// ignore IE throwing errors when focusing hidden elements
		// 		}
		// 	}
		// }
  });
</script>