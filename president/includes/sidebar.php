<?php
  $president_id = $_SESSION['session_president_id'];

  $account_query = "SELECT * FROM account_tbl A 
    INNER JOIN student_record_tbl B ON A.student_record_id = B.student_record_id
    INNER JOIN groups_tbl C ON A.group_id = C.group_id 
    WHERE A.account_id = ".$president_id."";

  $account_result = mysqli_query($con, $account_query);
  $account_fetch = mysqli_fetch_assoc($account_result);
  $first_name = $account_fetch['first_name'];
  $last_name = $account_fetch['last_name'];
  $profile_picture = $account_fetch['profile_picture'];
  $group_id = $account_fetch['group_id'];
  $group_name = $account_fetch['group_name'];
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../_assets/dist/img/cvsu.png" alt="PAG Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">PAG | President</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../_uploads/images/<?=$profile_picture;?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?=$first_name;?> <?=$last_name;?></a>
        </div>
      </div>

      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block"><?=$group_name;?></a>
        </div>
      </div>
        
      
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="index.php" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
          </a>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-user-cog"></i>
              <p>Account <i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="profile.php?account=<?=$president_id;?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile Settings</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="password.php?account=<?=$president_id;?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Password Settings</p>
                </a>
              </li>
            </ul>
        </li>

        <li class="nav-item">
          <a href="submitted_documents.php" class="nav-link">
          <i class="nav-icon fas fa-file-alt"></i> 
          <p>Documents</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="forms.php" class="nav-link">
            <i class="nav-icon fas fa-download"></i>
            <p> Downloadable Forms</p>
          </a>
        </li>

        <li class="nav-header">PRESIDENT SETTINGS</li>

        <li class="nav-item">
          <a href="manage_events.php?group=<?=$group_id;?>" class="nav-link">
            <i class="nav-icon fas fa-calendar-week"></i>
            <p>Events</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="news.php?group=<?=$group_id;?>" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>News</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="group_profile.php?group=<?=$group_id;?>" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>Group Profile</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="group_members.php?group=<?=$group_id;?>" class="nav-link">
            <i class="nav-icon fas fa-users"></i> 
            <p>Group Members</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="group_achievements.php?group=<?=$group_id;?>" class="nav-link">
            <i class="nav-icon fas fa-trophy"></i>
            <p>Achievements</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="group_gallery.php?group=<?=$group_id;?>" class="nav-link">
            <i class="nav-icon far fa-image"></i>
            <p>Gallery</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="footer_links.php" class="nav-link">
            <i class="nav-icon fas fa-link"></i>
            <p>Footer Links</p>
          </a>
        </li>
        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>