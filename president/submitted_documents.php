<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-file-alt"></i> Submitted Documents</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Documents</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="submit_document.php">
                <i class="fas fa-plus"></i> Submit Document
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Document</th>
                        <th>Date and Time Submitted</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $group_query = "SELECT group_id FROM account_tbl WHERE account_id = '".$_SESSION['session_president_id']."'";
                        $group_fetch_result = mysqli_query($con, $group_query);
                        $group_fetch = mysqli_fetch_assoc($group_fetch_result);
                        $group_id = $group_fetch['group_id'];

                        $documents_fetch_query = "SELECT * FROM documents_tbl WHERE group_id = '".$group_id."'";
                        $documents_fetch_result = mysqli_query($con, $documents_fetch_query);
                        while($row = mysqli_fetch_array($documents_fetch_result)) {
                          $datetime_sent = $row['date_sent'];
                          $date_sent = date('F d, Y', strtotime($datetime_sent));
                          $time_sent = date('h:i A', strtotime($datetime_sent));
                          $document = $row['document_name'];
                          $document = (strlen($document) > 45) ? substr($document,0,44).'...' : $document;     
                      ?>
                        <tr>
                          <td><?=$document;?></td>
                          <td>
                            <strong>Date:</strong> <?=$date_sent;?><br> 
                            <strong>Time:</strong> <?=$time_sent;?>
                          </td>
                          <td>
                            <a class="btn btn-warning btn-sm" href="view_document.php?document=<?=$row['document_id'];?>">
                              <i class="fas fa-eye"></i> View
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
            
                  <tfoot>
                    <tr>
                      <th>Document</th>
                      <th>Date and Time Submitted</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>