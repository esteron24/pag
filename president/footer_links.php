<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-link"></i> Manage Footer Links</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Links</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_link.php">
                <i class="fas fa-plus"></i> Add Link
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Link Name</th>
                        <th>Featured?</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $links_fetch_query = "SELECT * FROM other_link_tbl A 
                          INNER JOIN groups_tbl B ON A.group_id = B.group_id
                          WHERE A.group_id = '".$group_id."' ORDER BY A.link_id DESC";
                        $links_fetch_result = mysqli_query($con, $links_fetch_query);
                        while($row = mysqli_fetch_array($links_fetch_result)) {
                          $link = $row['link_name'];
                          $link = (strlen($link) > 45) ? substr($link,0,44).'...' : $link;   
                      ?>
                        <tr>
                          <td><?=$link;?></td>
                          <td>
                            <?php
                                echo'<select name="is_featured" id="'.$row['link_id'].'" class="form-control is_featured" style="width: 100%;">';
                                        if($row['is_featured'] == 0){
                                          echo'
                                            <option value="1">Yes</option>
                                            <option selected value="0">No</option>
                                          ';
                                        }else{
                                          echo'
                                            <option selected value="1">Yes</option>
                                            <option value="0">No</option>
                                          ';
                                        }
                                echo'</select>';
                            ?>
                          </td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_link.php?link=<?= $row['link_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_link" id="<?=$row['link_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                        <th>Link Name</th>
                        <th>Featured?</th>
                        <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Link------------------
  $(document).on('click', '.delete_link', function(){
      var delete_link = $(this).attr("id");
      var data = {      
        delete_link:delete_link
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this link?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "president_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Link successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="footer_links.php";
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });

//-------------------Set feature/unfeature file------------------
    $(document).on('change', '.is_featured', function(){
      var link_id = $(this).attr("id");
      var link_form = $(this).val();
      var data = {
        link_id:link_id,     
        link_form:link_form
      };
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "president_functions.php",
        dataType: "json",
        data: data,
        success: function(response){
          if(response == true){
            console.log("Success");
          }
        }
      });
    });
</script>