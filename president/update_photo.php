<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $photo = $_GET['photo'];
    $photo_query = "SELECT * FROM gallery_tbl WHERE photo_id =".$photo."";
    $photo_result = mysqli_query($con, $photo_query);
    $photo_fetch = mysqli_fetch_assoc($photo_result);

    $photo_id = $photo_fetch['photo_id'];
    $photo = $photo_fetch['photo'];
    $is_featured = $photo_fetch['is_featured'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="far fa-image"></i> Update Photo</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Gallery</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
            <form class="form-horizontal" id="gallery_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <!-- Uploaded image area-->
                  <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                  <div class="dimage" id="event-image">
                    <img id="imageResult" src="../_uploads/images/<?=$photo;?>" alt=" "> 
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Gallery Photo</label>
                    <div class="custom-file">
                    <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                      <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                    </div>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-lg-6">
                <!-- /.form-group -->
                <div class="form-group">
                    <label>Featured?</label>
                    <select name="is_featured" id="is_featured" class="form-control is_featured" style="width: 100%;">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <input type="hidden" name="photo" id="photo" value="<?=$photo_id;?>">
                      <a href="group_gallery.php?group=<?=$group_id;?>" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="update_photo" id="update_photo" value="Update Photo" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(this);
    });
  });

//AJAX form submission with Validation (UPDATE)
$().ready(function() {
    //JS set selected element in dropdown
    var is_featured = "<?php echo $is_featured ?>";
    $("#is_featured").val(is_featured).change();
    var group_id = "<?php echo $group_id ?>";

    $("#gallery_form").validate({
      submitHandler: function(form){
        var formData = new FormData(form);
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "president_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Photo successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="group_gallery.php?group="+group_id;
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>