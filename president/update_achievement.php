<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $achievement = $_GET['achievement'];
    $achievement_query = "SELECT * FROM achievements_tbl WHERE achievement_id =".$achievement."";
    $achievement_result = mysqli_query($con, $achievement_query);
    $achievement_fetch = mysqli_fetch_assoc($achievement_result);

    $achievement_title = $achievement_fetch['achievement_title'];
    $achievement_year = $achievement_fetch['achievement_year'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon fas fa-trophy"></i> Update Achievement</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Achievements</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" id="achievement_form" method="post" autocomplete="off">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <div class="form-group">
                <label>Achievement Title</label>
                <input type="text" name="achievement_title" id="achievement_title" class="form-control" value="<?=$achievement_title;?>" placeholder="Enter achievement title">
              </div>
              <!-- /.form-group -->

              <!-- /.form-group -->
              <div class="form-group">
                <label>Year Achieved</label>
                <input type="text" name="achievement_year" id="achievement_year" class="form-control" value="<?=$achievement_year;?>" placeholder="Enter year when the achievement received" data-toggle="datetimepicker" data-target="#achievement_year">
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                      <input type="hidden" name="group" id="group" value="<?=$group_id;?>">
                      <input type="hidden" name="achievement" id="achievement" value="<?=$achievement;?>">
                      <a href="group_achievements.php?group=<?=$group_id;?>" class="btn btn-secondary float-right">Cancel</a>
                      <input type="submit" name="update_achievement" id="update_achievement" value="Update Achievement" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $('#achievement_year').datetimepicker({
      viewMode: 'years',
      format: 'YYYY'
    })
  })

//AJAX form submission with Validation
  $().ready(function() {
    var group_id = "<?php echo $group_id ?>";
    $("#achievement_form").validate({
      rules: {
        achievement_title:{
          required: true,
          minlength: 10
        },
        achievement_year: "required"
      },
      messages: {
				achievement_title: {
          required: "Achievement title is required.",
          minlength: "Achievement title must be at least 10 characters long.",
        },
        achievement_year: "Achievement year is required."
			},
      submitHandler: function(form){
        var serializedData = $(form).serialize();
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update this achievement?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "president_functions.php",
                  data: serializedData,
                  dataType: "json",
                  type: "POST",
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Achievement successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="group_achievements.php?group="+group_id;
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>