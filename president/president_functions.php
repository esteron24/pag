<?php
  if (!isset($_SESSION['session_president_id'])) {
      session_start();
  }
  require '../connect.php';

  $president = $_SESSION['session_president_id'];

//Get group id
  $group_query = "SELECT group_id FROM account_tbl WHERE account_id = '".$president."'";
  $group_fetch_result = mysqli_query($con, $group_query);
  $group_fetch = mysqli_fetch_assoc($group_fetch_result);
  $group_id = $group_fetch['group_id'];

//Adding document
    if(isset($_POST["submit_document"])){
        $uploadDir = '../_uploads/documents/'; 
        $uploadedDocument = '';
        $document_name = mysqli_real_escape_string($con, $_POST["document_name"]);

        //Change filename upon upload
        $file  = basename($_FILES["document"]["name"]);
        $filename = str_replace(" ", "_", $document_name);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $newfilename = date('dmYHis').$filename.'.'.$extension;

        if(move_uploaded_file($_FILES["document"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedDocument = $newfilename; 
        }

        $submit_document_query = "INSERT INTO documents_tbl (group_id, document_name, document_file, date_sent) 
            VALUES ('$group_id', '$document_name', '$uploadedDocument', current_timestamp())";

        if(mysqli_query($con, $submit_document_query)) {
            echo "true"; 
            exit();
        }
    }

//Validate group name (Update) then, returns true when it is the existing group name
    if(isset($_POST["update_group_name"])){
        $group_name = mysqli_real_escape_string($con, $_POST["group_name"]);
        $db_group = mysqli_real_escape_string($con, $_POST["db_group"]);
        $validate_group_query = "SELECT * FROM groups_tbl WHERE group_name = '".$group_name."'";
        $group_result_check = mysqli_query($con, $validate_group_query);
        if(mysqli_num_rows($group_result_check) > 0 && $group_name !== $db_group){
            echo(json_encode(false));
        }else{
            echo(json_encode(true));
        }
    }

//Updating group
    if(isset($_POST["update_group"])){
        $uploadLogoDir = '../_uploads/logos/'; 
        $uploadedLogo = '';
    
        $uploadImagesDir = '../_uploads/images/'; 
        $uploadedCover = '';
    
        $group = mysqli_real_escape_string($con, $_POST["group"]);
        $group_name = mysqli_real_escape_string($con, $_POST["group_name"]);
        $group_description = mysqli_real_escape_string($con, $_POST["description"]);
        $embed_link = mysqli_real_escape_string($con, $_POST["embed_link"]);
    
        $newLogoFilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
        $newCoverFilename= date('dmYHis').str_replace(" ", "", basename($_FILES["cover_upload"]["name"]));
    
        $file_query = "SELECT group_logo, cover_image FROM groups_tbl WHERE group_id =".$group."";
        $file_result = mysqli_query($con, $file_query);
        $file_fetch = mysqli_fetch_assoc($file_result);
    
        $group_logo = $file_fetch['group_logo'];
        $cover_image = $file_fetch['cover_image'];
        
        //Check if any file is uploaded
        if (empty($_FILES["upload"]["name"])) {
            $uploadedLogo = $group_logo ;
        } else {
            $uploadedLogo = $newLogoFilename; 
        }
    
        if(empty($_FILES["cover_upload"]["name"])){
            $uploadedCover = $cover_image;
        } else {
            $uploadedCover = $newCoverFilename; 
        }
        
        $update_group_query = "UPDATE groups_tbl 
        SET group_logo = '".$uploadedLogo."',
            cover_image = '".$uploadedCover."',
            video_embed_link = '".$embed_link."',
            group_name = '".$group_name."',
            group_description = '".$group_description."',
            group_updated = current_timestamp()
        WHERE group_id = '".$group."'";
    
        if(mysqli_query($con, $update_group_query)) {
            move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadLogoDir.$newLogoFilename);
            move_uploaded_file($_FILES["cover_upload"]["tmp_name"], $uploadImagesDir.$newCoverFilename);
            echo "true"; 
            exit();
        }
    }

//Checks if the student number has a membership or not
    if(isset($_POST["validate_membership"])){
        $student_number = mysqli_real_escape_string($con, $_POST["student_number"]);

        $validate_membership_query = "SELECT * FROM members_tbl A 
            INNER JOIN student_record_tbl B ON A.student_record_id = B.student_record_id
            WHERE B.student_number = '".$student_number."'";
        $membership_result_check = mysqli_query($con, $validate_membership_query);
        $validate_studnum_query = "SELECT * FROM student_record_tbl WHERE student_number = '".$student_number."'";
        $studnum_result_check = mysqli_query($con, $validate_studnum_query);

        if(mysqli_num_rows($membership_result_check) > 0){
            echo(json_encode("This student number is already have a group membership."));
        }elseif(mysqli_num_rows($membership_result_check) == 0 && mysqli_num_rows($studnum_result_check) >= 1){
            echo(json_encode(true));
        }else{
            echo(json_encode("This student number is not existing in the system."));
        }
    }

//Adding member
    if(isset($_POST["add_member"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';
    
        $student_record = mysqli_real_escape_string($con, $_POST["record"]);
        $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
        if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedPicture = $newfilename; 
        }
        $add_member_query = "INSERT INTO members_tbl (student_record_id, profile_picture, group_id, date_joined) 
        VALUES ('$student_record', '$uploadedPicture', '$group_id', current_timestamp())";
        if(mysqli_query($con, $add_member_query)) {
            echo "true"; 
            exit();
        }
    }

// Delete member
    if (isset($_POST["delete_membership"])) {
        $member = mysqli_real_escape_string($con, $_POST["delete_membership"]);
        $delete_member = "DELETE FROM members_tbl WHERE member_id = '".$member."'";
        if(mysqli_query($con, $delete_member)) {
            echo "true"; 
            exit();
        }
    }

    //Updating member
    if(isset($_POST["update_member"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';    

        $member_id = mysqli_real_escape_string($con, $_POST["member"]);
        //Check if any file is uploaded
        if (empty($_FILES["upload"]["name"])){
            $update_member_query = "UPDATE members_tbl 
                SET group_id = '".$group_id."' 
            WHERE member_id = '".$member_id."'";
        } else {
            $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
            if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
                $uploadedPicture = $newfilename; 
            }
            $update_member_query = "UPDATE members_tbl 
                SET group_id = '".$group_id."',
                profile_picture = '".$uploadedPicture."' 
            WHERE member_id = '".$member_id."'";
        }
        if(mysqli_query($con, $update_member_query)) {
            echo "true"; 
            exit();
        }
    }

//Adding group achievement
    if(isset($_POST["add_achievement"])){
        $group = mysqli_real_escape_string($con, $_POST["group"]);
        $achievement_title = mysqli_real_escape_string($con, $_POST["achievement_title"]);
        $achievement_year = mysqli_real_escape_string($con, $_POST["achievement_year"]);
    
        $add_achievement_query = "INSERT INTO achievements_tbl (group_id, achievement_title, achievement_year) 
        VALUES ('$group', '$achievement_title', '$achievement_year')";
    
        if(mysqli_query($con, $add_achievement_query)) {
            echo "true"; 
            exit();
        }
    }

// Delete achievement
    if (isset($_POST["delete_achievement"])) {
        $achievement = mysqli_real_escape_string($con, $_POST["delete_achievement"]);
        $delete_achievement = "DELETE FROM achievements_tbl WHERE achievement_id = '".$achievement."'";
        if(mysqli_query($con, $delete_achievement)) {
            echo "true"; 
            exit();
        }
    }

//Updating achievement
    if(isset($_POST["update_achievement"])){
        $achievement = mysqli_real_escape_string($con, $_POST["achievement"]);
        $group = mysqli_real_escape_string($con, $_POST["group"]);
        $achievement_title = mysqli_real_escape_string($con, $_POST["achievement_title"]);
        $achievement_year = mysqli_real_escape_string($con, $_POST["achievement_year"]);
    
        $update_achievement_query = "UPDATE achievements_tbl 
        SET group_id = '".$group."',
        achievement_title = '".$achievement_title."',
        achievement_year = '".$achievement_year."'
        WHERE achievement_id = '".$achievement."'";
    
        if(mysqli_query($con, $update_achievement_query)) {
            echo "true"; 
            exit();
        }
    }

//Validate username (Update) then, returns true when it is his/her username
    if(isset($_POST["update_username"])){
        $username = mysqli_real_escape_string($con, $_POST["username"]);
        $db_username = mysqli_real_escape_string($con, $_POST["db_username"]);
        $validate_username_query = "SELECT * FROM account_tbl WHERE username = '".$username."'";
        $username_result_check = mysqli_query($con, $validate_username_query);
        if(mysqli_num_rows($username_result_check) > 0 && $username !== $db_username){
            echo(json_encode(false));
        }else{
            echo(json_encode(true));
        }
    }

//Updating president account
    if(isset($_POST["update_president_account"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';
    
        $account = mysqli_real_escape_string($con, $_POST["account"]);
        $username = mysqli_real_escape_string($con, $_POST["username"]);
    
    //Check if any file is uploaded
        if (empty($_FILES["upload"]["name"])){
            $update_president_query = "UPDATE account_tbl 
            SET username = '".$username."',
                group_id = '".$group_id."'
            WHERE account_id = '".$account."'";
        }else{
            //Change filename upon upload
            $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
            //Move filename upon upload
            if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
                $uploadedPicture = $newfilename; 
            }
            $update_president_query = "UPDATE account_tbl 
            SET profile_picture = '".$uploadedPicture."', 
                username = '".$username."',
                group_id = '".$group_id."'
            WHERE account_id = '".$account."'";
        }
            if(mysqli_query($con, $update_president_query)) {
                echo "true"; 
                exit();
            }
        }
  
//Validate if it is the current password of president
    if(isset($_POST["validate_password"])){
        $account = mysqli_real_escape_string($con, $_POST["db_account"]);
        $current = mysqli_real_escape_string($con, $_POST["current_password"]);
        $validate_password_query = "SELECT password FROM account_tbl WHERE account_id = '".$account."'";
        $account_result = mysqli_query($con, $validate_password_query);
        $password_fetch = mysqli_fetch_assoc($account_result);
        $db_current = $password_fetch['password'];
        if(password_verify($current, $db_current)){
            echo(json_encode(true));
        }else{
            echo(json_encode(false));
        }
    }

//Change Password
    if(isset($_POST["change_password"])){
        $account = mysqli_real_escape_string($con, $_POST["account"]);
        $new = mysqli_real_escape_string($con, $_POST["new_password"]);
        $hashed_password = password_hash($new, PASSWORD_DEFAULT);

        $change_password_query = "UPDATE account_tbl 
            SET password = '".$hashed_password."'
        WHERE account_id = '".$account."'";

        if(mysqli_query($con, $change_password_query)) {
            echo "true"; 
            exit();
        }
    }

//Adding news
    if(isset($_POST["add_news"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';
    
        $news_title = mysqli_real_escape_string($con, $_POST["news_title"]);
        $news_content = mysqli_real_escape_string($con, $_POST["news_content"]);
    
        //Change filename upon upload
        $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
        
        if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedPicture = $newfilename; 
        }
    
        $add_news_query = "INSERT INTO news_tbl (group_id, news_title, news_picture, news_content, news_date_created) 
        VALUES ('$group_id', '$news_title', '$uploadedPicture', '$news_content', current_timestamp())";
    
        if(mysqli_query($con, $add_news_query)) {
            echo "true"; 
            exit();
        }
    }

// Delete news
    if (isset($_POST["delete_news"])) {
        $news = mysqli_real_escape_string($con, $_POST["delete_news"]);
        $delete_news = "DELETE FROM news_tbl WHERE news_id = '".$news."'";
        if(mysqli_query($con, $delete_news)) {
            echo "true"; 
            exit();
        }
    }

//Updating news
    if(isset($_POST["update_news"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';
    
        $news = mysqli_real_escape_string($con, $_POST["news"]);
        $news_title = mysqli_real_escape_string($con, $_POST["news_title"]);
        $news_content = mysqli_real_escape_string($con, $_POST["news_content"]);
    
        //Check if any file is uploaded
        if (empty($_FILES["upload"]["name"])){
        $update_news_query = "UPDATE news_tbl 
            SET news_title = '".$news_title."',
            news_content = '".$news_content."'
            WHERE news_id = '".$news."'";
        } else {
        //Change filename upon upload
        $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
        //Move filename upon upload
        if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedPicture = $newfilename; 
        }
    
        $update_news_query = "UPDATE news_tbl 
            SET news_picture = '".$uploadedPicture."',
            news_title = '".$news_title."',
            news_content = '".$news_content."'
            WHERE news_id = '".$news."'";
        }
    
        if(mysqli_query($con, $update_news_query)) {
            echo "true"; 
            exit();
        }
    }

//Adding event
    if(isset($_POST["add_event"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';

        $event_title = mysqli_real_escape_string($con, $_POST["event_title"]);
        $event_location = mysqli_real_escape_string($con, $_POST["event_location"]);
        $event_date = mysqli_real_escape_string($con, $_POST["event_date"]);;
        $event_description = mysqli_real_escape_string($con, $_POST["event_description"]);
    
        $new_date = date("Y-m-d",strtotime($event_date));

        //Change filename upon upload
        $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
        
        if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedPicture = $newfilename; 
        }
  
        
        $add_event_query = "INSERT INTO events_tbl (group_id, event_title, event_picture, event_location, event_date, 
            event_description) VALUES ('$group_id', '$event_title', '$uploadedPicture', '$event_location', '$new_date',  
            '$event_description')";
  
        if(mysqli_query($con, $add_event_query)) {
            echo "true"; 
            exit();
        }
    }

// Delete event
    if (isset($_POST["delete_event"])) {
        $event = mysqli_real_escape_string($con, $_POST["delete_event"]);
        $delete_event = "DELETE FROM events_tbl WHERE event_id = '".$event."'";
        if(mysqli_query($con, $delete_event)) {
            echo "true"; 
            exit();
        }
    }

//Update event
    if(isset($_POST["update_event"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';
        
        $event = mysqli_real_escape_string($con, $_POST["event"]);
        $group = mysqli_real_escape_string($con, $_POST["group"]);
        $event_title = mysqli_real_escape_string($con, $_POST["event_title"]);
        $event_location = mysqli_real_escape_string($con, $_POST["event_location"]);
        $event_date = mysqli_real_escape_string($con, $_POST["event_date"]);
        $event_description = mysqli_real_escape_string($con, $_POST["event_description"]);

        $new_date = date("Y-m-d",strtotime($event_date));
        
        //Check if any file is uploaded
        if (empty($_FILES["upload"]["name"])){
            $update_event_query = "UPDATE events_tbl 
            SET group_id = '".$group_id."',
                event_title = '".$event_title."',
                event_location = '".$event_location."',
                event_date = '".$new_date."',
                event_description = '".$event_description."'
            WHERE event_id = '".$event."'";
        } else {
            //Change filename upon upload
            $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
            //Move filename upon upload
            if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedPicture = $newfilename; 
            }
            $update_event_query = "UPDATE events_tbl 
            SET group_id = '".$group_id."',
                event_picture = '".$uploadedPicture."',
                event_title = '".$event_title."',
                event_location = '".$event_location."',
                event_date = '".$new_date."',
                event_description = '".$event_description."'
            WHERE event_id = '".$event."'";
        }
        
        if(mysqli_query($con, $update_event_query)) {
            echo "true"; 
            exit();
        }
    }

//Adding photo
    if(isset($_POST["add_photo"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';

        $feature_state = mysqli_real_escape_string($con, $_POST["is_featured"]);
        $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
        if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedPicture = $newfilename; 
        }
        $add_photo_query = "INSERT INTO gallery_tbl (group_id, photo, date_uploaded, is_featured) 
        VALUES ('$group_id', '$uploadedPicture', current_timestamp(), '$feature_state')";
        if(mysqli_query($con, $add_photo_query)) {
            echo "true"; 
            exit();
        }
    }

//Updating photo
    if(isset($_POST["update_photo"])){
        $uploadDir = '../_uploads/images/'; 
        $uploadedPicture = '';
        $photo_id = mysqli_real_escape_string($con, $_POST["photo"]);
        $feature_state = mysqli_real_escape_string($con, $_POST["is_featured"]);

        //Check if any file is uploaded
        if (empty($_FILES["upload"]["name"])){
            $update_photo_query = "UPDATE gallery_tbl 
                SET is_featured = '".$feature_state."'
            WHERE photo_id = '".$photo_id."'";
        } else {
            $newfilename= date('dmYHis').str_replace(" ", "", basename($_FILES["upload"]["name"]));
            if(move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadDir.$newfilename)){ 
                $uploadedPicture = $newfilename; 
            }
            $update_photo_query = "UPDATE gallery_tbl 
                SET is_featured = '".$feature_state."',
                photo = '".$uploadedPicture."',
                date_uploaded = current_timestamp() 
            WHERE photo_id = '".$photo_id."'";
        }
        if(mysqli_query($con, $update_photo_query)) {
            echo "true"; 
            exit();
        }
    }

// Delete photo
    if (isset($_POST["delete_photo"])) {
        $photo = mysqli_real_escape_string($con, $_POST["delete_photo"]);
        $delete_photo = "DELETE FROM gallery_tbl WHERE photo_id = '".$photo."'";
        if(mysqli_query($con, $delete_photo)) {
            echo "true"; 
            exit();
        }
    }

//Adding form
    if(isset($_POST["add_form"])){
        $uploadDir = '../_uploads/documents/'; 
        $uploadedDocument = '';
        $document_name = mysqli_real_escape_string($con, $_POST["document_name"]);

        //Change filename upon upload
        $file  = basename($_FILES["document"]["name"]);
        $filename = str_replace(" ", "_", $document_name);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $newfilename = date('dmYHis').$filename.'.'.$extension;

        if(move_uploaded_file($_FILES["document"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedDocument = $newfilename; 
        }

        $add_form_query = "INSERT INTO forms_tbl (form_type_id, group_id, form_name, form_file, date_uploaded) 
            VALUES (2, '$group_id', '$document_name', '$uploadedDocument', current_timestamp())";

        if(mysqli_query($con, $add_form_query)) {
            echo "true"; 
            exit();
        }
    }

//Updating form
    if(isset($_POST["update_form"])){
    $uploadDir = '../_uploads/documents/'; 
    $uploadedDocument = '';

    $form = mysqli_real_escape_string($con, $_POST["form"]);
    $document_name = mysqli_real_escape_string($con, $_POST["document_name"]);

    //Check if any file is uploaded
    if (empty($_FILES["document"]["name"])){
        $update_form_query = "UPDATE forms_tbl 
        SET form_type_id = 2,
            group_id = '".$group_id."',
            form_name = '".$document_name."'  
        WHERE form_id = '".$form."'";
    } else {
        //Change filename upon upload
        $file  = basename($_FILES["document"]["name"]);
        $filename = str_replace(" ", "_", $document_name);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $newfilename = date('dmYHis').$filename.'.'.$extension;

        if(move_uploaded_file($_FILES["document"]["tmp_name"], $uploadDir.$newfilename)){ 
            $uploadedDocument = $newfilename; 
        }

        $update_form_query = "UPDATE forms_tbl 
        SET form_type_id = 2,
            group_id = '".$group_id."',
            form_name = '".$document_name."',
            form_file = '".$uploadedDocument."',
            date_uploaded = current_timestamp()  
        WHERE form_id = '".$form."'";
    }
        if(mysqli_query($con, $update_form_query)) {
            echo "true"; 
            exit();
        }
    }


// Feature form
    if (isset($_POST["feature_form"])) {
        $form = mysqli_real_escape_string($con, $_POST["form_id"]);
        $is_featured = mysqli_real_escape_string($con, $_POST["feature_form"]);
        $feature_form = "UPDATE forms_tbl SET is_featured = '".$is_featured."' WHERE form_id = '".$form."'";
        if(mysqli_query($con, $feature_form)) {
            echo "true"; 
            exit();
        }
    }


// Delete form
    if (isset($_POST["delete_form"])) {
        $form = mysqli_real_escape_string($con, $_POST["delete_form"]);
        $delete_form = "DELETE FROM forms_tbl WHERE form_id = '".$form."'";
        if(mysqli_query($con, $delete_form)) {
            echo "true"; 
            exit();
        }
    }

//Adding link
    if(isset($_POST["add_link"])){
        $link_name = mysqli_real_escape_string($con, $_POST["link_name"]);
        $link_url = mysqli_real_escape_string($con, $_POST["link_url"]);

        $add_link_query = "INSERT INTO other_link_tbl (group_id, link_name, link_url, is_featured) 
            VALUES ('$group_id', '$link_name', '$link_url', 1)";

        if(mysqli_query($con, $add_link_query)) {
            echo "true"; 
            exit();
        }
    }

//Updating link
    if(isset($_POST["update_link"])){
        $link_id = mysqli_real_escape_string($con, $_POST["link_id"]);
        $link_name = mysqli_real_escape_string($con, $_POST["link_name"]);
        $link_url = mysqli_real_escape_string($con, $_POST["link_url"]);

        $update_link_query = "UPDATE other_link_tbl 
            SET group_id = '".$group_id."',
            link_name = '".$link_name."',
            link_url = '".$link_url."'
            WHERE link_id = '".$link_id."'";

        if(mysqli_query($con, $update_link_query)) {
            echo "true"; 
            exit();
        }
    }

// Feature link
    if (isset($_POST["link_form"])) {
        $link = mysqli_real_escape_string($con, $_POST["link_id"]);
        $is_featured = mysqli_real_escape_string($con, $_POST["link_form"]);
        $link_form = "UPDATE other_link_tbl SET is_featured = '".$is_featured."' WHERE link_id = '".$link."'";
        if(mysqli_query($con, $link_form)) {
            echo "true"; 
            exit();
        }
    }

// Delete link
    if (isset($_POST["delete_link"])) {
        $link = mysqli_real_escape_string($con, $_POST["delete_link"]);
        $delete_link = "DELETE FROM other_link_tbl WHERE link_id = '".$link."'";
        if(mysqli_query($con, $delete_link)) {
            echo "true"; 
            exit();
        }
    }

?>