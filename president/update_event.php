<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $event = $_GET['event'];
    $event_query = "SELECT * FROM events_tbl WHERE event_id =".$event."";
    $event_result = mysqli_query($con, $event_query);
    $event_fetch = mysqli_fetch_assoc($event_result);

    $group = $event_fetch['group_id'];
    $event_title = $event_fetch['event_title'];
    $event_location = $event_fetch['event_location'];
    $event_date = $event_fetch['event_date'];
    $event_description = $event_fetch['event_description'];
    $event_image = $event_fetch['event_picture'];

    $new_date = date('m/d/Y', strtotime($event_date));
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-calendar-week"></i> Update Event</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Events</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" id="event_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

            <div class="row">
              <div class="col-12 col-sm-6">
                <div class="form-group">
                  <!-- Uploaded image area-->
                  <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                  <div class="dimage" id="event-image">
                    <img id="imageResult" src="../_uploads/images/<?=$event_image;?>" alt=""> 
                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-lg-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Event Photo</label>
                    <div class="custom-file">
                    <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                      <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                    </div>
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-lg-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Event Title</label>
                  <input type="text" name="event_title" id="event_title" value="<?=$event_title;?>" placeholder="Enter event title" class="form-control">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>

            <div class="row">
              <div class="col-lg-6">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Event Location</label>
                  <input type="text" name="event_location" id="event_location" value="<?=$event_location;?>" placeholder="Enter event location" class="form-control">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Event Date</label>
                    <input type="text" name="event_date" id="event_date"  value="<?=$new_date;?>" placeholder="Enter event date" class="form-control" data-toggle="datetimepicker" data-target="#event_date">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <div class="col-lg-12">
                <div class="description_item">
                    <label>Event Description</label>
                    <textarea name="event_description" id="event_description" class="form-control" placeholder="Enter event description" rows="6"><?=$event_description;?></textarea>
                </div>
              </div>
            </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                    <input type="hidden" name="group" id="group" value="<?=$group_id;?>">
                    <input type="hidden" name="event" id="event" value="<?=$event;?>">
                    <a href="manage_events.php?group=<?=$group_id;?>" class="btn btn-secondary float-right">Cancel</a>
                    <input type="submit" name="update_event" id="update_event" value="Update Event" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>

$(function () {

//Initialize CKEditor Elements
  // CKEDITOR.replace('event_description');

//Initialize Select2 Elements
  $('.select2').select2()

  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  //Datemask2 mm/dd/yyyy
  $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
  //Money Euro
  $('[data-mask]').inputmask()

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({
  timePicker: true,
  timePickerIncrement: 30,
  locale: {
      format: 'MM/DD/YYYY hh:mm A'
  }
  })

  var event_date = $('#event_date').val();
  var new_event_date = moment(event_date, "MM-DD-YYYY");

  $('#event_date').datetimepicker({
    format: 'L',
    minDate: new Date(),
    defaultDate: new_event_date
  })
})

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
  });

//Initialize WYSIWYG to textarea using TinyMCE
  // tinymce.init({
  //   selector: '#event_description',
  //   height: '400',
  //   onchange_callback: function(editor) {
	// 		tinyMCE.triggerSave();
	// 		$("#" + editor.id).valid();
	// 	}
  // });

//AJAX form submission with Validation
  $().ready(function() {
    var group_id = "<?php echo $group_id ?>";
    $("#event_form").validate({
      // ignore: "",
      rules:{
        event_title: "required",
        event_location: "required",
        event_datetime_start: "required",
        event_datetime_end: "required",
        event_description:{
          required: true,
          minlength: 100,
        }
      },
      messages:{
        event_title: "Event title is required",
        event_location: "Event location is required",
        event_datetime_start: "Starting date and time is required",
        event_datetime_end: "Ending date and time  is required",
        event_description:{
          required: "Event description is required",
          minlength: "Event description must be at least 100 characters long",
        }
      },
      // errorPlacement: function(label, element) {
			// 	// position error label after generated textarea
			// 	if (element.is("textarea")) {
			// 		label.insertAfter(element.next());
			// 	} else {
			// 		label.insertAfter(element)
			// 	}
			// },
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        var formData = new FormData(form);
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update this event?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "president_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Event successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="manage_events.php?group="+group_id;
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });

    // validator.focusInvalid = function() {
		// 	// put focus on tinymce on submit validation
		// 	if (this.settings.focusInvalid) {
		// 		try {
		// 			var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
		// 			if (toFocus.is("textarea")) {
		// 				tinyMCE.get(toFocus.attr("id")).focus();
		// 			} else {
		// 				toFocus.filter(":visible").focus();
		// 			}
		// 		} catch (e) {
		// 			// ignore IE throwing errors when focusing hidden elements
		// 		}
		// 	}
		// }
  });


</script>