<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">President Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <?php
                $count_members_query = "SELECT COUNT(*) AS members FROM members_tbl WHERE group_id = ".$group_id."";
                $members_result = mysqli_query($con, $count_members_query);
                $members_fetch = mysqli_fetch_assoc($members_result);
                $total_members = $members_fetch['members'];
              ?>
                <h3><?=$total_members;?></h3>
                <p>Total Members</p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <a href="group_members.php?group=<?=$group_id;?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <?php
                $count_events_query = "SELECT COUNT(*) AS events FROM events_tbl WHERE group_id = ".$group_id."";
                $events_result = mysqli_query($con, $count_events_query);
                $events_fetch = mysqli_fetch_assoc($events_result);
                $total_events = $events_fetch['events'];
              ?>
                <h3><?=$total_events;?></h3>
                <p>Events Created</p>
              </div>
              <div class="icon">
                <i class="fas fa-calendar-week"></i>
              </div>
              <a href="manage_events.php?group=<?=$group_id;?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <?php
                $count_news_query = "SELECT COUNT(*) AS news FROM news_tbl WHERE group_id = ".$group_id."";
                $news_result = mysqli_query($con, $count_news_query);
                $news_fetch = mysqli_fetch_assoc($news_result);
                $total_news = $news_fetch['news'];
              ?>
                <h3><?=$total_news;?></h3>
                <p>News Created</p>
              </div>
              <div class="icon">
                <i class="fas fa-newspaper"></i>
              </div>
              <a href="news.php?group=<?=$group_id;?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              <?php
                $count_documents_query = "SELECT COUNT(*) AS documents FROM documents_tbl WHERE group_id = ".$group_id."";
                $documents_result = mysqli_query($con, $count_documents_query);
                $documents_fetch = mysqli_fetch_assoc($documents_result);
                $total_documents = $documents_fetch['documents'];
              ?>
                <h3><?=$total_documents;?></h3>
                <p>Documents Submitted</p>
              </div>
              <div class="icon">
                <i class="fas fa-file-alt"></i>
              </div>
              <a href="submitted_documents.php?group=<?=$group_id;?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>