<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="nav-icon far fa-image"></i> Group Gallery</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Gallery</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" href="add_photo.php">
                <i class="fas fa-plus"></i> Add Photo
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive ">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Photo</th>
                        <th>Date Uploaded</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $group_id = $_GET['group'];
                        $gallery_fetch_query = "SELECT * FROM gallery_tbl A 
                          INNER JOIN groups_tbl B ON A.group_id = B.group_id 
                        WHERE A.group_id = '".$group_id."'";
                        $gallery_fetch_result = mysqli_query($con, $gallery_fetch_query);
                        while($row = mysqli_fetch_array($gallery_fetch_result)) {
                          $date_uploaded = $row['date_uploaded'];
                          $new_date = date('F d, Y', strtotime($date_uploaded));
                          $photo = $row['photo'];
                          $photo = (strlen($photo) > 45) ? substr($photo,0,44).'...' : $photo;
                      ?>
                        <tr>
                          <td><?=$photo;?></td>
                          <td><?=$new_date;?></td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_photo.php?photo=<?=$row['photo_id'];?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_photo" id="<?=$row['photo_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                        <th>Photo</th>
                        <th>Date Uploaded</th>
                        <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Photo------------------
$(document).on('click', '.delete_photo', function(){
      var group_id = "<?php echo $group_id ?>";
      var delete_photo = $(this).attr("id");
      var data = {      
        delete_photo:delete_photo
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this photo?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "president_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Photo successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="group_gallery.php?group="+group_id;
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });
</script>