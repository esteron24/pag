<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $account = $_GET['account'];
    $account_query = "SELECT * FROM account_tbl A 
        INNER JOIN student_record_tbl B ON A.student_record_id = B.student_record_id
    WHERE A.account_id=".$account."";

    $account_result = mysqli_query($con, $account_query);
    $account_fetch = mysqli_fetch_assoc($account_result);
    $profile_picture = $account_fetch['profile_picture'];

    $group = $account_fetch['group_id'];
    $username = $account_fetch['username'];
    $fname = $account_fetch['first_name'];
    $lname = $account_fetch['last_name'];
    $current_pass = $account_fetch['password'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-user-edit"></i> Profile Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Account</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content 1-->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <form class="form-horizontal" id="president_form" method="post" autocomplete="off" enctype="multipart/form-data">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

            <div class="row">
                <div class="col-12 col-sm-6">
                 <!-- /.form-group -->
                 <div class="form-group">
                    <!-- Uploaded image area-->
                    <label class="font-italic text-black text-center">The image uploaded will be rendered inside the container below.</label>
                    <div class="profile-image" id="profile-image">
                        <img id="imageResult" src="../_uploads/images/<?=$profile_picture;?>" alt=""> 
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>

            <!-- /.form-group -->
            <div class="form-group">
                <label>Profile Picture</label>
                <div class="custom-file">
                <input type="file" onchange="readURL(this);" class="form-control" name="upload" id="upload" accept="image/*">
                <!-- <label class="custom-file-label" for="upload">Choose file</label> -->
                </div>
            </div>
            <!-- /.form-group -->  

            <!-- /.form-group -->
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="full_name" id="full_name" value="<?=$fname;?> <?=$lname;?>" class="form-control" disabled>
            </div>
            <!-- /.form-group -->

            <!-- /.form-group -->
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" id="username" value="<?=$username;?>" class="form-control" placeholder="Enter username">
                <span id='availability'></span>
            </div>
            <!-- /.form-group -->

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                    <input type="hidden" name="account" id="account" value="<?=$account;?>">
                    <a href="index.php" class="btn btn-secondary float-right">Cancel</a>
                    <input type="submit" name="update_president_account" id="update_president_account" value="Update Profile" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
$(document).ready(function () {
  bsCustomFileInput.init();
});

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#imageResult')
              .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
      readURL(input);
    });
  });



//AJAX form submission with Validation
$().ready(function() {
  var db_username = "<?php echo $username ?>";

    $("#president_form").validate({
      rules:{
        username:{
          required: true,
          minlength: 5,
          remote: {
            url: "president_functions.php",
            type: "post",
            data: {
              update_username: function() {
                return username;
              },
              db_username: function() {
                return db_username;
              },
            }
          }
        },
      },
      messages:{
        username:{
          required: "Username is required.",
          minlength: "Username must be at least 5 characters long.",
          remote: "Username already exists."
        }
      },
      submitHandler: function(form){
        // var serializedData = $(form).serialize();
        // event.preventDefault();
        
        var formData = new FormData(form);

        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update your profile?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "president_functions.php",
                  data: formData,
                  dataType: "json",
                  type: "POST",
                  processData: false,
                  contentType: false,
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Profile successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                              window.location.href="index.php";
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>