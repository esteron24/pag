<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>
<?php
    $account = $_GET['account'];
    $account_query = "SELECT * FROM account_tbl WHERE account_id=".$account."";
    $account_result = mysqli_query($con, $account_query);
    $account_fetch = mysqli_fetch_assoc($account_result);
    $current_pass = $account_fetch['password'];
    $access_level = $account_fetch['access_level_id'];
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-key"></i> Password Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Account</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
        <form class="form-horizontal" id="password_form" method="post" autocomplete="off">
            <div class="card-header">
              <h3 class="card-title">Please fill up the fields accurately.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Current Password</label>
                  <input type="password" name="current_password" id="current_password" class="form-control" placeholder="Enter current password">
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label>New Password</label>
                  <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter new password">
                </div>
                <!-- /.form-group -->

                <!-- /.form-group -->
                <div class="form-group">
                  <label>Re-type New Password</label>
                  <input type="password" name="re_password" id="re_password" class="form-control" placeholder="Re-type new password">
                </div>
                <!-- /.form-group -->

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="row">
                  <div class="col-12">
                    <input type="hidden" name="account" id="account" value="<?=$account;?>">
                    <a href="index.php" class="btn btn-secondary float-right">Cancel</a>
                    <input type="submit" name="change_password" id="change_password" value="Update Password" class="btn btn-success float-right" style="margin-right: 10px;"> 
                  </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
//AJAX form submission with Validation
  $().ready(function() {
    // var current_password = $('#current_password').val();
    var db_account = $('#account').val();

    $("#password_form").validate({
      rules: {
        current_password:{
          required: true,
          remote: {
            url: "president_functions.php",
            type: "post",
            data: {
              validate_password: function() {
                return current_password;
              },
              db_account: function() {
                return db_account;
              }
            }
          }
        },
        new_password:{
          required: true,
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/
        },
        re_password:{
          required: true,
          pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
          equalTo: "#new_password"
        }
      },
      messages: {
        current_password:{
          required: "Current password is required.",
          remote: "Current password is incorrectly typed."
        },
        new_password:{
          required: "New password is required.",
          pattern: "Password must be minimum of eight characters, at least one uppercase letter, one lowercase letter and one number."
        },
        re_password: {
          required: "Please re-type the new password again.",
          pattern: "Password must be minimum of eight characters, at least one uppercase letter, one lowercase letter and one number.",
          equalTo: "Password does not match."
        },
      },
      submitHandler: function(form){
        var serializedData = $(form).serialize();
        // event.preventDefault();
        $.confirm({
          icon: 'fas fa-exclamation-triangle',
          title: 'Attention',
          content: 'Are you sure you want to update your password?',
          type: 'orange',
          buttons: {
            confirm: {
              closeIcon: true,
              btnClass: 'btn-orange',
              action: function(){
                $.ajax({
                  url: "president_functions.php",
                  data: serializedData,
                  dataType: "json",
                  type: "POST",
                  success: function(response){
                    if(response == true){
                      $.confirm({
                        icon: 'fas fa-check',
                        title: 'Success',
                        content: 'Password successfully updated!',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                               window.location.href="index.php"; 
                            }
                        }
                      });
                    }
                  }
                });
              }
            },
            cancel: function () {
              //Cancel AJAX Request
            }
          }
        });
      },
      highlight: function(element){
        $(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function(element){
        $(element).closest('.form-control').removeClass('is-invalid');
      }
    });
  });
</script>



