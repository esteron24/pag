<?php include 'includes/header.php'; ?>
<?php include 'includes/navbar.php'; ?>
<?php include 'includes/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1><i class="fas fa-users"></i> Group Members</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Membership</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <a class="btn btn-success btn-md" data-toggle="modal" data-target="#validate_membership_modal" href="#">
                <i class="fas fa-plus"></i> Add Member
              </a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th>Student Number</th>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Date Joined</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                
                  <tbody>
                      <?php
                        $group_id = $_GET['group'];
                        $members_fetch_query = "SELECT * FROM members_tbl A
                          INNER JOIN student_record_tbl B ON A.student_record_id = B.student_record_id
                          INNER JOIN courses_tbl C ON B.course_id = C.course_id
                          INNER JOIN groups_tbl D ON A.group_id = D.group_id
                          WHERE A.group_id = '".$group_id."'";
                        $members_fetch_result = mysqli_query($con, $members_fetch_query);
                        while($row = mysqli_fetch_array($members_fetch_result)) {
                          $date_joined = $row['date_joined'];
                          $date = date('F d, Y', strtotime($date_joined));
                      ?>
                        <tr>
                          <td><?=$row['student_number'];?></td>
                          <td><?=$row['first_name']." ".$row['last_name'];?></td>
                          <td><?=$row['course_acronym'];?></td>
                          <td><?=$date;?></td>
                          <td>
                            <a class="btn btn-success btn-sm" href="update_member.php?member=<?= $row['member_id']; ?>">
                              <i class="fas fa-edit"></i> Update
                            </a>
                            <a class="btn btn-danger btn-sm delete_membership" id="<?=$row['member_id'];?>" href="#">
                              <i class="fas fa-trash"></i> Delete
                            </a>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>                
                  </tbody>
                
                  <tfoot>
                    <tr>
                      <th>Student Number</th>
                      <th>Name</th>
                      <th>Course</th>
                      <th>Date Joined</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<?php include 'includes/modal.php'; ?>
<?php include 'includes/footer.php'; ?>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

//-------------------Delete Membership------------------
    $(document).on('click', '.delete_membership', function(){
      var group_id = "<?php echo $group_id ?>";
      var delete_membership = $(this).attr("id");
      var data = {      
        delete_membership:delete_membership
      };
      event.preventDefault();

        $.confirm({
        icon: 'fas fa-exclamation-triangle',
        title: 'Attention',
        content: 'Are you sure you want to delete this member?',
        type: 'orange',
        buttons: {
          confirm: {
            closeIcon: true,
            btnClass: 'btn-orange',
            action: function(){
              $.ajax({
                type: "POST",
                url: "president_functions.php",
                dataType: "json",
                data: data,
                success: function(response){
                  if(response == true){
                    $.confirm({
                      icon: 'fas fa-check',
                      title: 'Success',
                      content: 'Member successfully deleted!',
                      type: 'green',
                      typeAnimated: true,
                      buttons: {
                          close: function () {
                            window.location.href="group_members.php?group="+group_id;
                          }
                      }
                    });
                  }
                }
              });
            }
          },
          cancel: function () {
            //Cancel AJAX Request
          }
        }
      });
    });

//AJAX form submission with Validation
    $().ready(function() {
        $("#validate_membership").validate({
            rules: {
                student_number:{
                    required: true,
                    number: true,
                    minlength: 9,
                    maxlength: 9,
                    remote: {
                    url: "president_functions.php",
                    type: "post",
                    data: {
                        validate_membership: function() {
                        return student_number;
                        },
                    }
                    }
                },
            },
            messages: {
                student_number: {
                    required: "Student number is required.",
                    number: "Please enter a valid student number.",
                    minlength: "Student number must be 9 numbers long.",
                    maxlength: "Student number must be 9 numbers long.",
                },
            },
            submitHandler: function(form){
                var serializedData = $(form).serializeArray(), dataObj = {};

                $(serializedData).each(function(i, field){
                    dataObj[field.name] = field.value;
                });

                var student = dataObj['student_number'];
                window.location.href="add_member.php?student="+student;
            },
            highlight: function(element){
                $(element).closest('.form-control').addClass('is-invalid');
            },
            unhighlight: function(element){
                $(element).closest('.form-control').removeClass('is-invalid');
                $(element).closest('.form-control').addClass('is-valid');
            }
        });
    });
</script>