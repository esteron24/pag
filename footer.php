<!-- Footer Section Start -->
<footer class="footer-area section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.2s">
          <h3><img style="width: 120px; height: auto;" src="_landing_assets/img/cvsu.png" alt=""></h3>
          <p>Cavite State University Main-Campus Performing Arts Group</p>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
          <h3>QUICK LINKS</h3>
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="events.php">Upcoming Events</a></li>
            <li><a href="news.php">Latest News</a></li>
          </ul>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.6s">
          <h3>OTHER LINKS</h3>
          <ul>
            <?php
              $link_query = "SELECT * FROM other_link_tbl WHERE is_featured = 1";
              $link_fetch_result = mysqli_query($con, $link_query);
              while($row = mysqli_fetch_array($link_fetch_result)) {
                  echo '
                    <li><a href="'.$row['link_url'].'">'.$row['link_name'].'</a></li>
                  ';
              }
            ?>
          </ul>
        </div>
        <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.8s">
            <h3>FOLLOW US ON</h3>
            <ul class="footer-social">
              <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
              <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
              <!-- <li><a class="linkedin" href="#"><i class="lni-linkedin-filled"></i></a></li> -->
              <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>
            </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer Section End -->