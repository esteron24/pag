<!DOCTYPE html>
<html lang="en">
<?php
    require 'connect.php';
?>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Performing Arts Group | Home</title>
    <link rel="shortcut icon" href="_landing_assets/img/cvsu.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/fonts/line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/slicknav.css">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/nivo-lightbox.css" >
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="_landing_assets/css/responsive.css">

  </head>
  <style>
      .nav-link{
          color: black !important;
      }
  </style>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <a href="index.php" class="navbar-brand"><img style="width: 20%; height: auto;" src="_landing_assets/img/cvsu.png" alt=""></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                <a class="nav-link" href="index.php">
                  Home
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Groups
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php
                        $group_query = "SELECT * FROM groups_tbl";
                        $group_fetch_result = mysqli_query($con, $group_query);
                        while($row = mysqli_fetch_array($group_fetch_result)) {
                            echo '<a class="dropdown-item" href="group_single.php?group='.$row['group_id'].'">'.$row['group_name'].'</a>';
                        }
                    ?>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">
                  About
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="events.php">
                  Events
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="news.php">
                  News
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="login.php">
                  Log In
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="index.html#team">
                  Contact
                </a>
              </li> -->
              <li class="nav-item">
                <a class="nav-link" href="files.php">
                  Download
                </a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
          <li>
            <a class="page-scroll" href="index.php">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Groups
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php
                    $group_query = "SELECT * FROM groups_tbl";
                    $group_fetch_result = mysqli_query($con, $group_query);
                    while($row = mysqli_fetch_array($group_fetch_result)) {
                        echo '<a class="dropdown-item" href="group_single.php?group='.$row['group_id'].'">'.$row['group_name'].'</a>';
                    }
                ?>
            </div>
          </li>
          <li>
             <a class="page-scroll" href="about.php">About</a>
          </li>
          <li>
            <a class="page-scroll" href="events.php">Events</a>
          </li>
          <li>
            <a class="page-scroll" href="news.php">News</a>
          </li>
          <li>
            <a class="page-scroll" href="login.php">Log In</a>
          </li>
          <!-- <li>
            <a class="page-scroll" href="#team">Contact</a>
          </li> -->
          <li>
            <a class="page-scroll" href="files.php">Download</a>
          </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->
    </header>

     <!-- Event Slides Section Start -->
    <section id="event-slides" class="news-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">About Us</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Cavite State University - Don Severino delas Alas Campus</p>
            </div>
          </div>
          <div class="padding-section wow fadeInLeft" data-wow-delay="0.3s">
            <h2 class="intro-title">History</h2>
            <p class="intro-desc">
              The Cavite State University (CvSU) has its humble beginnings in 1906 as the Indang Intermediate School with 
              the American Thomasites as the first teachers. Several transformations in the name of the school took place; 
              Indang Farm School in 1918, Indang Rural High School in 1927, and Don Severino National Agriculture School in 
              1958. In 1964, the school was converted into a State College by virtue of Republic Act 3917 and became known as 
              Don Severino Agricultural College (DSAC).
            </p>
            <p class="intro-desc">
              On January 22, 1998, by virtue of Republic Act No.8468, DSAC was converted into Cavite State University. In 2001, 
              Cavite College of Fisheries (CACOF) in Naic, Cavite and Cavite College of Arts and Trade (CCAT) in Rosario, Cavite, 
              were integrated to the University by virtue of CHED Memo No. 27 s. 2000. Since then, additional campuses in the 
              province were established through memoranda of agreement with the LGUs. At present, CvSU has 11 campuses in the 
              province. The main campus, approximately 70 hectares in land area and named as Don Severino delas Alas Campus, is in 
              Indang, Cavite.
            </p>
            <p class="intro-desc">
              CvSU is mandated “to provide excellent, equitable and relevant educational opportunities in the arts, sciences and 
              technology through quality instruction, and responsive research and development activities. It shall produce 
              professional, skilled and morally upright individuals for global competitiveness.”
            </p>
            <p class="intro-desc">
              The University is offering close to 100 curricular programs in the undergraduate and graduate levels. It has more than 
              25,000 students and 1,200 faculty and staff from all campuses. The University is also authorized to certify and confer 
              appropriate academic degrees in accordance with the Expanded Tertiary Education and Accreditation Program (ETEEAP). It 
              has been accredited by TESDA as Trade and Testing Venue, and designated by the Department of Agriculture as the 
              National Center for Research and Development for Coffee and Urban Agriculture. It also hosts the Southern Tagalog 
              Agriculture Research and Development Consortium (STARDEC), the Affiliated Renewable Energy Center for Region IV-A, and 
              the Regional ICT Center for Region IV-A.
            </p>
            <p class="intro-desc">
              CvSU adheres to its commitment to Truth, Excellence and Service as it aims to be the “premier University in historic 
              Cavite recognized for excellence in the development of globally competitive and morally upright individuals”.
            </p>

            <h2 class="intro-title">Vision</h2>
            <p class="intro-desc">
              The premier university in historic Cavite recognized for excellence in the development of globally competitive and morally 
              upright individuals.
            </p>

            <h2 class="intro-title">Mission</h2>
            <p class="intro-desc">
              Cavite State University shall provide excellent, equitable and relevant educational opportunities in the arts, sciences and 
              technology through quality instruction and responsive research and development activities. It shall produce professional, 
              skilled and morally upright individuals for global competitiveness.
            </p>
          </div>
          <!-- <div class="col-md-6 col-lg-6 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
            <div class="video">
              <img class="img-fluid" src="_landing_assets/img/about/about.jpg" alt="">
            </div>
          </div> -->
          
        </div>
      </div>
    </section>
    <!-- Event Slides Section End -->
                   
      <?php include 'footer.php'; ?>

      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="site-info">
                <p>© Designed and Developed by <a href="http://uideck.com" rel="nofollow">UIdeck</a></p>
              </div>      
            </div>
          </div>
        </div>
      </div>

      <!-- Go to Top Link -->
      <a href="#" class="back-to-top">
        <i class="lni-chevron-up"></i>
      </a>

      <div id="preloader">
        <div class="sk-circle">
          <div class="sk-circle1 sk-child"></div>
          <div class="sk-circle2 sk-child"></div>
          <div class="sk-circle3 sk-child"></div>
          <div class="sk-circle4 sk-child"></div>
          <div class="sk-circle5 sk-child"></div>
          <div class="sk-circle6 sk-child"></div>
          <div class="sk-circle7 sk-child"></div>
          <div class="sk-circle8 sk-child"></div>
          <div class="sk-circle9 sk-child"></div>
          <div class="sk-circle10 sk-child"></div>
          <div class="sk-circle11 sk-child"></div>
          <div class="sk-circle12 sk-child"></div>
        </div>
      </div>

      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="_landing_assets/js/jquery-min.js"></script>
      <script src="_landing_assets/js/popper.min.js"></script>
      <script src="_landing_assets/js/bootstrap.min.js"></script>
      <script src="_landing_assets/js/jquery.countdown.min.js"></script>
      <script src="_landing_assets/js/jquery.nav.js"></script>
      <script src="_landing_assets/js/jquery.easing.min.js"></script>
      <script src="_landing_assets/js/wow.js"></script>
      <script src="_landing_assets/js/jquery.slicknav.js"></script>
      <script src="_landing_assets/js/nivo-lightbox.js"></script>
      <script src="_landing_assets/js/main.js"></script>
      <script src="_landing_assets/js/form-validator.min.js"></script>
      <script src="_landing_assets/js/contact-form-script.min.js"></script>
      <script src="_landing_assets/js/map.js"></script>
      <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      
    </body>
  </html>
